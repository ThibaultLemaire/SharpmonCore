namespace Sharpmon.Interfaces

open System
open System.IO

type IRandom =
    abstract member Next : maxValue: int -> int
    abstract member Next : minValue: int * maxValue: int -> int

type IConsoleDisplay =
    abstract member Title: string with set
    abstract member KeyAvailable: bool with get

    abstract member ReadKey: intercept: bool -> ConsoleKeyInfo
    abstract member ReadKey: unit -> ConsoleKeyInfo
    abstract member ReadLine: unit -> string
    abstract member Write: string -> unit
    abstract member WriteLine: unit -> unit
    abstract member WriteLine: string -> unit
    abstract member WriteLine: format: string * 'a -> unit
    abstract member WriteLine: format: string * 'a * 'b -> unit
    abstract member WriteLine: color: ConsoleColor * string -> unit
    abstract member WriteLine: color: ConsoleColor * format: string * 'a -> unit
    abstract member WriteLine: color: ConsoleColor * format: string * 'a * 'b -> unit
    abstract member WriteLine: color: ConsoleColor * format: string * 'a * 'b * 'c -> unit
    abstract member Clear: unit -> unit

type 'a IPersister =
    abstract member Save: saveName: string * 'a -> unit
    abstract member Load: saveName: string -> 'a
    abstract member Has: saveName: string -> bool
    abstract member Delete: saveName: string -> unit

type 'a IStreamSerializer =
    abstract member SerializeTo: writer: StreamWriter * object: 'a -> unit
    abstract member DeserializeFrom: reader: StreamReader -> 'a

type IFactory<'a, 'b> =
    abstract member Deconstruct: subject: 'a -> 'b
    abstract member Reconstruct: material: 'b -> 'a

type IShopItem =
    abstract member Name: string with get
    abstract member Description: string with get
    abstract member Price: int with get
