using Ninject;
using Xunit;
using Sharpmon.DI;
using Sharpmon.Interfaces;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;

namespace Sharpmon.Tests
{
    public class DI_Tests
    {
        IKernel testKernel = DependencyInjection.Kernel();

        [Fact]
        public void Can_Inject_IRandom()
        {
            var random = testKernel.Get<IRandom>();
            
            random.Should().NotBeNull();
        }

        [Fact]
        public void Can_Inject_AppName()
        {
            var appName = testKernel.Get<string>("AppName");
            
            appName.Should().NotBeNull();
        }

        [Fact]
        public void Can_Inject_IPersister()
        {
            var persister = testKernel.Get<IPersister<Player>>();
            
            persister.Should().NotBeNull();
        }

        [Fact]
        public void Can_Inject_Attack_Register()
        {
            var register = testKernel.Get<Register<Attack>>();
            
            register.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public void Can_Inject_Sharpmon_Register()
        {
            var register = testKernel.Get<Register<Sharpmon>>();
            
            register.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public void Every_ShopItem_Should_Correspond_To_An_Item()
        {
            var shopItems = testKernel.Get<IEnumerable<IShopItem>>();

            shopItems
                .Select(x => testKernel.Get<Item>(x.Name))
                .Should()
                .BeEquivalentTo(testKernel.Get<IEnumerable<Item>>());
        }
    }
}
