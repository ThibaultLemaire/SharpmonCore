using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Sharpmon.Factories;
using Sharpmon.Interfaces;
using Sharpmon.Wrappers;
using Xunit;

namespace Sharpmon.Tests
{
    public class PlayerFactory_Tests
    {
        [Theory, AutoMoqData]
        public void Deconstruct_Then_Reconstruct_Makes_Clone(
            string name,
            List<Sharpmon> sharpmons,
            List<Item> items,
            int money,
            string difficulty,
            IConsoleDisplay consoleDisplay,
            Register<Attack> availableAttacks
        )
        {
            var random = new RandomWrapper();
            var availableItems = items.ToDictionary(x => x.Name);
            var input = new Player(
                name,
                sharpmons,
                sharpmons[random.Next(sharpmons.Count)],
                money,
                availableItems,
                difficulty,
                random,
                consoleDisplay
            );
            var factory = new PlayerFactory(
                random,
                consoleDisplay,
                x => availableItems[x],
                new SharpmonFactory(
                    consoleDisplay,
                    random,
                    availableAttacks)
            );

            var output = factory.Reconstruct(factory.Deconstruct(input));

            output.Should().BeEquivalentTo(input);
            output.Should().NotBeSameAs(input);
        }
    }
}
