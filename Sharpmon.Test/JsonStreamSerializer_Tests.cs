using FluentAssertions;
using Sharpmon.FSharp;
using Sharpmon.Interfaces;
using Xunit;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Sharpmon.Tests
{
    public class JsonStreamSerializer_Tests
    {
        public class Bom
        {
            public double Shiva;
            public int Shankara;
            public string Bolenat;
        }

        struct BomRepr
        {
            public readonly double a;
            public readonly int o;

            public BomRepr(double a, int o)
            {
                this.a = a;
                this.o = o;
            }
        }

        class BomFactory : IFactory<Bom, BomRepr>
        {
            string bolenat;

            public BomFactory(string bolenat)
            {
                this.bolenat = bolenat;
            }

            public BomRepr Deconstruct(Bom subject)
            {
                return new BomRepr(subject.Shiva / 4, subject.Shankara << 1);
            }

            public Bom Reconstruct(BomRepr material)
            {
                return new Bom
                {
                    Shiva = material.a * 4,
                    Shankara = material.o >> 1,
                    Bolenat = bolenat,
                };
            }
        }

        [Theory, AutoMoqData]
        public void Serialize_Deserialize_Should_Be_Identity(
            Bom input
        )
        {
            var writeStream = new MemoryStream();
            var factory = new BomFactory(input.Bolenat);
            IStreamSerializer<Bom> serializer = new JsonStreamSerializer<Bom, BomRepr>(factory);

            serializer.SerializeTo(new StreamWriter(writeStream), input);
            var readStream = new MemoryStream(writeStream.ToArray());
            var output = serializer.DeserializeFrom(new StreamReader(readStream));

            output.Should().BeEquivalentTo(input);
            output.Should().NotBeSameAs(input);
        }
    }
}
