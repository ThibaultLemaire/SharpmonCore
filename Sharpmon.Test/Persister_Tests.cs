using System;
using System.IO;
using FluentAssertions;
using Sharpmon.FSharp;
using Sharpmon.Interfaces;
using Xunit;
using Moq;

namespace Sharpmon.Tests
{
    public class Persister_Tests
    {
        string testDirectory = Directory.GetCurrentDirectory();

        [Theory, AutoMoqData]
        public void Save_Should_Create_File(
            string saveName,
            object testData,
            IStreamSerializer<object> serializer
        )
        {
            IPersister<object> testPersister = new Persister<object>(testDirectory, serializer);
            
            testPersister.Save(saveName, testData);

            var expectedFile = ExpectedFile(saveName);
            File.Exists(expectedFile).Should().BeTrue();
            File.Delete(expectedFile);
        }

        [Theory, AutoMoqData]
        public void Save_Should_Create_Folder(
            string saveName,
            string subdirectory,
            object testData,
            IStreamSerializer<object> serializer
        )
        {
            var subPath = Path.Combine(testDirectory, subdirectory);
            Directory.Exists(subPath).Should().BeFalse();
            IPersister<object> testPersister = new Persister<object>(subPath, serializer);
            
            testPersister.Save(saveName, testData);

            Directory.Exists(subPath).Should().BeTrue();
            Directory.Delete(subdirectory, recursive: true);
        }
        
        [Theory, AutoMoqData]
        public void Has_Should_Return_True(
            string saveName,
            IStreamSerializer<object> serializer
        )
        {
            IPersister<object> testPersister = new Persister<object>(testDirectory, serializer);
            var expectedFile = ExpectedFile(saveName);
            File.Create(expectedFile);

            testPersister.Has(saveName).Should().BeTrue();

            File.Delete(expectedFile);
        }

        [Theory, AutoMoqData]
        public void Has_Should_Return_False(
            string nonExistantSave,
            IStreamSerializer<object> serializer
        )
        {
            IPersister<object> testPersister = new Persister<object>(testDirectory, serializer);

            testPersister.Has(nonExistantSave).Should().BeFalse();
        }
        
        [Theory, AutoMoqData]
        public void Delete_Should_Delete_File(
            string saveName,
            IStreamSerializer<object> serializer
        )
        {
            IPersister<object> testPersister = new Persister<object>(testDirectory, serializer);
            File.Create(ExpectedFile(saveName));

            testPersister.Delete(saveName);

            File.Exists(ExpectedFile(saveName)).Should().BeFalse();
        }
        
        [Theory, AutoMoqData]
        public void Delete_Should_Fail_Silently(
            string nonExistantSave,
            IStreamSerializer<object> serializer
        )
        {
            IPersister<object> testPersister = new Persister<object>(testDirectory, serializer);

            testPersister.Delete(nonExistantSave);
        }

        string ExpectedFile(string saveName)
        {
            return Path.Combine(testDirectory, $"{saveName}.save.json");
        }

        void CleanUp(string saveName)
        {
            File.Delete(ExpectedFile(saveName));
        }
    }
}
