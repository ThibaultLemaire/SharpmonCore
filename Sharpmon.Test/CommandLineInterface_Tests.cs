using Xunit;
using FluentAssertions;
using Sharpmon.Interfaces;
using System;
using System.Collections.Generic;
using Moq;
using AutoFixture;
using Sharpmon.FSharp;

namespace Sharpmon.Tests
{
    public class CommandLineInterface_Tests
    {
        readonly Fixture fixture = new AutoMoqFixture();
        readonly CommandLineInterface testCLI;
        readonly Mock<IConsoleDisplay> mockConsole = new Mock<IConsoleDisplay>();
        ItemByName mockItemByName;

        Item ItemByNameDispatch(string name) => mockItemByName(name);

        public CommandLineInterface_Tests()
        {
            testCLI = new CommandLineInterface(
                mockConsole.Object,
                fixture.Create<string>(),
                fixture.Create<Version>(),
                fixture.Create<IRandom>(),
                fixture.Create<Persister<Player>>(),
                fixture.Create<Register<Sharpmon>>(),
                fixture.Create<List<IShopItem>>(),
                ItemByNameDispatch,
                fixture.Create<NewPlayerBuilder>()
            );
        }

        [Theory, AutoMoqData]
        public void BuyItem_With_Quantity_Zero_Should_Do_Nothing(
            Player player,
            IShopItem item
        )
        {
            var startingMoney = player.Money;
            var startingItems = new Dictionary<string, Item>(player.Items);

            testCLI.BuyItem(player, item, 0);

            mockConsole.Verify(x => x.WriteLine("You didn't buy anything."));
            mockConsole.VerifyNoOtherCalls();
            player.Money.Should().Be(startingMoney);
            player.Items.Should().BeEquivalentTo(startingItems);
        }

        [Theory, AutoMoqData]
        public void BuyItem_With_Not_Enough_Money_Should_Do_Nothing(
            Player player,
            int quantity
        )
        {
            var startingMoney = player.Money;
            var startingItems = new Dictionary<string, Item>(player.Items);
            var mockItem = new Mock<IShopItem>();
            mockItem.SetupGet(x => x.Price).Returns(startingMoney + 1);

            testCLI.BuyItem(player, mockItem.Object, quantity);

            mockConsole.Verify(x => x.WriteLine(ConsoleColor.Red, "Not enough Money"));
            mockConsole.VerifyNoOtherCalls();
            player.Money.Should().Be(startingMoney);
            player.Items.Should().BeEquivalentTo(startingItems);
        }

        [Theory, AutoMoqData]
        public void BuyItem_With_Enough_Money_Should_Buy_Quantity(
            Item item,
            int price,
            int quantity,
            int excess
        )
        {
            item.Count = 0;
            mockItemByName = _ => item;
            var player = fixture
                .Build<Player>()
                .With(x => x.Money, price * quantity + excess)
                .Create();
            var mockShopItem = new Mock<IShopItem>();
            mockShopItem.SetupGet(x => x.Price).Returns(price);
            mockShopItem.SetupGet(x => x.Name).Returns(item.Name);

            testCLI.BuyItem(player, mockShopItem.Object, quantity);

            player.Money.Should().Be(excess);
            player.Items.Values.Should().Contain(item);
            player.Items[item.Name].Count.Should().Be(quantity);
            mockConsole.Verify(x => x.WriteLine(
                ConsoleColor.DarkYellow, "You bought {0} {1}", quantity, item.Name));
            mockConsole.VerifyNoOtherCalls();
        }

        [Theory, AutoMoqData]
        public void BuyItem_With_Not_Enough_Money_Should_Buy_Max(
            Item item,
            int price,
            int quantity,
            int excess,
            int excessItems
        )
        {
            item.Count = 0;
            mockItemByName = _ => item;
            var smallExcess = excess % price;
            var player = fixture
                .Build<Player>()
                .With(x => x.Money, price * quantity + smallExcess)
                .Create();
            var mockShopItem = new Mock<IShopItem>();
            mockShopItem.SetupGet(x => x.Price).Returns(price);
            mockShopItem.SetupGet(x => x.Name).Returns(item.Name);

            testCLI.BuyItem(player, mockShopItem.Object, quantity + excessItems);

            player.Money.Should().Be(smallExcess);
            player.Items.Values.Should().Contain(item);
            player.Items[item.Name].Count.Should().Be(quantity);
            mockConsole.Verify(x => x.WriteLine(
                ConsoleColor.DarkYellow, "You bought {0} {1}", quantity, item.Name));
            mockConsole.VerifyNoOtherCalls();
        }
    }
}
