using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;
using Sharpmon.Interfaces;
using System.Linq;
using AutoFixture.Xunit2;

namespace Sharpmon.Tests
{
    public class Player_FT
    {
        Fixture fixture = new AutoMoqFixture();
        Mock<IConsoleDisplay> mockConsole = new Mock<IConsoleDisplay>();
        Mock<IRandom> mockRandom = new Mock<IRandom>();

        public Player_FT()
        {
            fixture.Register<Player>(() =>
                new Player(
                    name: fixture.Create<string>(),
                    sharpmons: fixture.Create<List<Sharpmon>>(),
                    currentSharpmon: fixture.Create<Sharpmon>(),
                    money: fixture.Create<int>(),
                    items: new Dictionary<string, Item> {{"Sharpball", fixture.Create<Item>()}},
                    difficulty: fixture.Create<string>(),
                    random: mockRandom.Object,
                    console: mockConsole.Object
                ));
        }

        [Theory, AutoMoqData]
        public void Capture_Should_Consume_Sharpball(
            int expectedCount,
            Sharpmon target
        )
        {
            // Arrange
            var testPlayer = fixture.Create<Player>();
            testPlayer.Items["Sharpball"].Count = expectedCount + 1;

            // Act
            testPlayer.Capture(target);

            // Assert
            testPlayer.Items["Sharpball"].Count.Should().Be(expectedCount);
            mockConsole.Verify(x => x.WriteLine(ConsoleColor.White, "You used Sharpball on {0}", target));
        }

        [Fact]
        public void Capture_Failure_Should_Do_Nothing()
        {
            // Arrange
            var testPlayer = fixture.Create<Player>();
            var target = fixture.Build<Sharpmon>()
                .With(x => x.MaxHP, 100)
                .With(x => x.HP, 25)
                .Create();
            mockRandom.Setup(x => x.Next(100)).Returns(26);

            // Act
            bool success = testPlayer.Capture(target);

            // Assert
            success.Should().BeFalse();
            mockConsole.Verify(
                x => x.WriteLine(ConsoleColor.Blue, "You failed to capture {0}.", target));
            testPlayer.Sharpmons.Should().NotContain(target);
        }

        [Fact]
        public void Capture_Success_Should_Add_To_Sharpmons()
        {
            // Arrange
            var testPlayer = fixture.Create<Player>();
            var target = fixture.Build<Sharpmon>()
                .With(x => x.MaxHP, 100)
                .With(x => x.HP, 25)
                .Create();
            mockRandom.Setup(x => x.Next(100)).Returns(24);

            // Act
            bool success = testPlayer.Capture(target);

            // Assert
            success.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(ConsoleColor.Cyan, "{0} has been added to your party!", target));
            testPlayer.Sharpmons.Should().Contain(target);
        }
    }
}
