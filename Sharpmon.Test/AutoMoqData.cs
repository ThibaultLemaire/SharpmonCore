using AutoFixture.Xunit2;

namespace Sharpmon.Tests
{
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute() : base(() => new AutoMoqFixture()) {}
    }
}
