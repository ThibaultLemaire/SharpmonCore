using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class Potion_FT : ItemFunctionalTest
    {
        public Potion_FT() : base("Potion") {}

        [Fact]
        public void Potion_Should_Heal_5_HP()
        {
            // Arrange
            int expectedHP = 5;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.HP, 0)
                .With(x => x.MaxHP, expectedHP)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.HP.Should().Be(expectedHP);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.Green,
                    $"{testSubject.Name} is restored {expectedHP} HP"),
                Times.Once);
        }

        [Fact]
        public void Potion_Should_Heal_To_Full_Health()
        {
            // Arrange
            int expectedHP = 3;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.HP, 0)
                .With(x => x.MaxHP, expectedHP)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.HP.Should().Be(expectedHP);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.Green,
                    $"{testSubject.Name} is restored {expectedHP} HP"),
                Times.Once);
        }
    }
}
