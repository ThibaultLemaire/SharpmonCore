using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class AccuracySeed_FT : ItemFunctionalTest
    {
        public AccuracySeed_FT() : base("Accuracy Seed") {}

        [Fact]
        public void Accuracy_Seed_Should_Add_1_To_Base_Accuracy()
        {
            // Arrange
            int expectedBaseAccuracy = 3;
            int increment = 1;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.BaseAccuracy, expectedBaseAccuracy - increment)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.BaseAccuracy.Should().Be(expectedBaseAccuracy);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkYellow,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Accuracy)} permanently increased by {increment}."),
                Times.Once);
        }
    }
}
