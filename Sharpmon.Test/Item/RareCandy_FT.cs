using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class RareCandy_FT : ItemFunctionalTest
    {
        public RareCandy_FT() : base("Rare Candy") {}

        [Theory, AutoMoqData]
        public void Rare_Candy_Should_Make_Target_Level_Up(
            int expectedLvl
        )
        {
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.Level, expectedLvl - 1)
                .Create();

            bool hasEffect = testItem.Activate(testSubject);

            testSubject.Level.Should().Be(expectedLvl);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.Cyan,
                    $"{testSubject.Name} levels up!\n{testSubject.Name} is now level {expectedLvl}"),
                Times.Once);
        }
    }
}
