using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class SuperPotion_FT : ItemFunctionalTest
    {
        public SuperPotion_FT() : base("Super Potion") {}

        [Fact]
        public void Super_Potion_Should_Heal_10_HP()
        {
            // Arrange
            int expectedHP = 10;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.HP, 0)
                .With(x => x.MaxHP, expectedHP)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.HP.Should().Be(expectedHP);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.Green,
                    $"{testSubject.Name} is restored {expectedHP} HP"),
                Times.Once);
        }

        [Fact]
        public void Super_Potion_Should_Heal_To_Full_Health()
        {
            // Arrange
            int expectedHP = 3;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.HP, 0)
                .With(x => x.MaxHP, expectedHP)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.HP.Should().Be(expectedHP);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.Green,
                    $"{testSubject.Name} is restored {expectedHP} HP"),
                Times.Once);
        }
    }
}
