using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class PowerSeed_FT : ItemFunctionalTest
    {
        public PowerSeed_FT() : base("Power Seed") {}

        [Fact]
        public void Power_Seed_Should_Add_1_To_Base_Power()
        {
            // Arrange
            int expectedBasePower = 3;
            int increment = 1;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.BasePower, expectedBasePower - increment)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.BasePower.Should().Be(expectedBasePower);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkRed,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Power)} permanently increased by {increment}."),
                Times.Once);
        }
    }
}
