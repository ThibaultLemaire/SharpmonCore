using AutoFixture;
using Moq;
using Xunit;
using Ninject;
using Sharpmon.Interfaces;
using Sharpmon.DI.Modules;
using Ninject.Modules;

namespace Sharpmon.Tests
{
    public abstract class ItemFunctionalTest
    {
        protected Fixture fixture = new AutoMoqFixture();
        protected Mock<IConsoleDisplay> mockConsole = new Mock<IConsoleDisplay>();
        protected readonly Item testItem;

        protected ItemFunctionalTest(string itemKey)
        {
            var kernel = new StandardKernel(
                new ItemTestsModule(mockConsole.Object),
                new ConsoleEventDisplayModule(),
                new RandomModule(),
                new ItemsModule());
            testItem = kernel.Get<ItemByName>()(itemKey);
        }
    }

    class ItemTestsModule : NinjectModule
    {
        readonly IConsoleDisplay mockDisplay;

        internal ItemTestsModule(IConsoleDisplay mockDisplay)
        {
            this.mockDisplay = mockDisplay;
        }

        public override void Load()
        {
            Bind<IConsoleDisplay>().ToConstant(mockDisplay);
        }
    }
}
