using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class MegaSeed_FT : ItemFunctionalTest
    {
        public MegaSeed_FT() : base("Mega Seed") {}

        [Fact]
        public void Mega_Seed_Should_Add_1_To_All_Stats()
        {
            // Arrange
            int expectedBaseAccuracy = fixture.Create<int>();
            int expectedBaseDefense = fixture.Create<int>();
            int expectedBaseDodge = fixture.Create<int>();
            int expectedBasePower = fixture.Create<int>();
            int expectedBaseSpeed = fixture.Create<int>();
            int increment = 1;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.BaseAccuracy, expectedBaseAccuracy - increment)
                .With(x => x.BaseDefense, expectedBaseDefense - increment)
                .With(x => x.BaseDodge, expectedBaseDodge - increment)
                .With(x => x.BasePower, expectedBasePower - increment)
                .With(x => x.BaseSpeed, expectedBaseSpeed - increment)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.BaseAccuracy.Should().Be(expectedBaseAccuracy);
            testSubject.BaseSpeed.Should().Be(expectedBaseSpeed);
            testSubject.BasePower.Should().Be(expectedBasePower);
            testSubject.BaseDodge.Should().Be(expectedBaseDodge);
            testSubject.BaseDefense.Should().Be(expectedBaseDefense);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkYellow,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Accuracy)} permanently increased by {increment}."),
                Times.Once);
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkGreen,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Defense)} permanently increased by {increment}."),
                Times.Once);
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkMagenta,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Dodge)} permanently increased by {increment}."),
                Times.Once);
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkRed,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Power)} permanently increased by {increment}."),
                Times.Once);
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkCyan,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Speed)} permanently increased by {increment}."),
                Times.Once);
        }
    }
}
