using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class DefenseSeed_FT : ItemFunctionalTest
    {
        public DefenseSeed_FT() : base("Defense Seed") {}

        [Fact]
        public void Defense_Seed_Should_Add_1_To_Base_Defense()
        {
            // Arrange
            int expectedBaseDefense = 3;
            int increment = 1;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.BaseDefense, expectedBaseDefense - increment)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.BaseDefense.Should().Be(expectedBaseDefense);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkGreen,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Defense)} permanently increased by {increment}."),
                Times.Once);
        }
    }
}
