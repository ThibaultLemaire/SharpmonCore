using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class SpeedSeed_FT : ItemFunctionalTest
    {
        public SpeedSeed_FT() : base("Speed Seed") {}

        [Fact]
        public void Speed_Seed_Should_Add_1_To_Base_Speed()
        {
            // Arrange
            int expectedBaseSpeed = 3;
            int increment = 1;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.BaseSpeed, expectedBaseSpeed - increment)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.BaseSpeed.Should().Be(expectedBaseSpeed);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkCyan,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Speed)} permanently increased by {increment}."),
                Times.Once);
        }
    }
}
