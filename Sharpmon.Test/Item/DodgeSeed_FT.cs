using System;
using Xunit;
using FluentAssertions;
using AutoFixture;
using Moq;

namespace Sharpmon.Tests
{
    public class DodgeSeed_FT : ItemFunctionalTest
    {
        public DodgeSeed_FT() : base("Dodge Seed") {}

        [Fact]
        public void Dodge_Seed_Should_Add_1_To_Base_Dodge()
        {
            // Arrange
            int expectedBaseDodge = 3;
            int increment = 1;
            Sharpmon testSubject = fixture
                .Build<Sharpmon>()
                .With(x => x.BaseDodge, expectedBaseDodge - increment)
                .Create();

            // Act
            bool hasEffect = testItem.Activate(testSubject);

            // Assert
            testSubject.BaseDodge.Should().Be(expectedBaseDodge);
            hasEffect.Should().BeTrue();
            mockConsole.Verify(
                x => x.WriteLine(
                    ConsoleColor.DarkMagenta,
                    $"{testSubject.Name}'s {nameof(Sharpmon.Dodge)} permanently increased by {increment}."),
                Times.Once);
        }
    }
}
