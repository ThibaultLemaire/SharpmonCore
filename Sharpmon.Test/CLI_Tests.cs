using Xunit;
using FluentAssertions;
using Sharpmon.FSharp;
using System.Collections.Generic;
using Ninject;
using Sharpmon.DI.Modules;
using Sharpmon.Interfaces;
using System.Linq;

namespace Sharpmon.Tests
{
    public class CLI_Tests
    {
        [Fact]
        public void ItemsInBoxes_Represents_Items_In_Ascii_Boxes()
        {
            var kernel = new StandardKernel(
                new ItemsModule()
            );
            var items = kernel
                .Get<IEnumerable<IShopItem>>()
                .Where(x =>
                       x.Name == "Sharpball"
                    || x.Name == "Rare Candy"
                    || x.Name == "Accuracy Seed");
            
            var actual = CLI.itemsInBoxes(items);

            actual.Should().Be(
                  "+--------------------------+\n"
                + "| Accuracy Seed      id: 3 |\n"
                + "| Price: #1000             |\n"
                + "| Permanently increases    |\n"
                + "| the Accuracy of a        |\n"
                + "| Sharpmon by 1.           |\n"
                + "+--------------------------+\n"
                + "| Rare Candy         id: 2 |\n"
                + "| Price: #700              |\n"
                + "| Makes a Sharpmon level   |\n"
                + "| up                       |\n"
                + "+--------------------------+\n"
                + "| Sharpball          id: 1 |\n"
                + "| Price: #150              |\n"
                + "| Used to capture          |\n"
                + "| Sharpmons                |\n"
                + "+--------------------------+"
            );
        }
    }
}
