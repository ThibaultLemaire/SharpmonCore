using Xunit;
using Sharpmon.Interfaces;
using FluentAssertions;
using System;
using Sharpmon.Wrappers;
using AutoFixture;

namespace Sharpmon.Tests
{
    public class Sharpmon_Tests
    {
        readonly Fixture fixture = new AutoMoqFixture();

        [Theory, AutoMoqData]
        public void XP_Overload_Makes_Sharpmon_LevelUp_Twice(
            int startingLevel,
            int xpToNextLevel
        )
        {
            var xpToLevelAfter = (int)Math.Ceiling(xpToNextLevel * (startingLevel + 1) * 1.3);
            var sharpmon = fixture.Build<Sharpmon>()
                            .With(x => x.Level, startingLevel)
                            .With(x => x.XP, 0)
                            .With(x => x.XPToNextLevel, xpToNextLevel)
                            .Create();
            var xpOverload = xpToNextLevel + xpToLevelAfter;

            sharpmon.XP += xpOverload;

            sharpmon.Level.Should().Be(startingLevel + 2);
        }
    }
}
