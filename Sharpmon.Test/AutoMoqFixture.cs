using AutoFixture;
using AutoFixture.AutoMoq;
using System.Linq;

namespace Sharpmon.Tests
{
    public class AutoMoqFixture : Fixture
    {
        public AutoMoqFixture() : base()
        {
            Customize(new AutoMoqCustomization());

            var attacks = Enumerable.Empty<Attack>();
            this.Register(() =>
            {
                var newAttacks = this.CreateMany<Attack>();
                attacks = attacks.Concat(newAttacks);
                return newAttacks.ToList();
            });
            this.Register(() => new Register<Attack>(attacks));

            var items = Enumerable.Empty<Item>();
            this.Register(() =>
            {
                var newItems = this.CreateMany<Item>();
                items = items.Concat(newItems);
                return newItems.ToList();
            });
        }
    }
}
