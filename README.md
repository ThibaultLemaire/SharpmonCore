# [Sharpmon.NETCore][Sharpmon.NETCore]

*Sharpmon* is a very simple and basic Role-Playing Game (RPG),
designed to be played in a Command-Line Interface (CLI).
It is freely inspired by the *Pokémon* game series.

## Gameplay

The game is a succession of so-called *screens*, that will guide the player through the game, giving him choices about what to do next.

These screens are usually pretty self-explanatory.

### Main Menu

![Main Menu screenshot](.screenshots/MainMenu.png)

### Fight

![Fight screenshot](.screenshots/Fight.png)

### Shop

![Shop screenshot](.screenshots/Shop.png)
![Shop Buy screenshot](.screenshots/Shop-Buy.png)

### Sharpmon Center

![Sharpmon Center screenshot](.screenshots/SharpmonCenter.png)

## Installation

### Download

To directly download a build for your OS go to the [Tags][Tags] page, choose the latest version (at the top of the list), click on the drop-down at the right hand side of your screen, and choose the version for your OS.

![Drop Down screenshot](.screenshots/DropDown.png)

The ***portable*** version is **lighter** and **runs on any platform** provided you have installed the [.NET Core framework][NETCoreAbout]. (See [Dependencies](#dependencies))

#### Run the portable version

In order to run this particular version, you just have to use the following command (make sure you are in the folder you just downloaded): 

``` shell
    dotnet Sharpmon.NETCore.dll
```


### Dependencies

This project is built upon the [.NET Core framework][NETCoreAbout], so if you are downloading the *portable* version or [building from source](#building-from-source), make sure to

> **[Install .NET core][NETCoreInstall]**. (Select the right OS)

### Building from source

First of all, you need the [.NET Core framework][NETCoreAbout]. (See [Dependencies](#dependencies))

Then you must [clone][CLIBasics] or [download](Download) this repo.

``` shell
git clone git@gitlab.com:ThibaultLemaire/SharpmonCore.git
```

Then `cd` into the cloned directory's `Sharpmon` folder

``` shell
cd SharpmonCore/Sharpmon
```

And just type

``` shell
dotnet run
```

This will automatically build and run the project for you. The game will launch and you will be greeted with the standard welcome screen.

Enjoy!

## Trivia

While *Sharpmon* is the name of the game, *Sharpmon.NETCore* is the name of its implementation with the [.NET core framework][NETCoreAbout] (i.e. this project).

I initially began *Sharpmon* as my 2nd year C# school project in 2016.  
(Back then I knew nothing of version control, which is why there is no history for that period.)

In 2017, with the introduction of .NET core 2.0, and because I had since dropped Windows for Debian, I decided to port that project over to [.NET core][NETCoreAbout].

[Download]: https://gitlab.com/ThibaultLemaire/SharpmonCore/repository/master/archive.tar.gz
[Sharpmon.NETCore]: https://gitlab.com/ThibaultLemaire/SharpmonCore
[CLIBasics]: https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html
[NETCoreAbout]: https://docs.microsoft.com/en-us/dotnet/framework/get-started/net-core-and-open-source
[NETCoreInstall]: https://www.microsoft.com/net/download
[Tags]: https://gitlab.com/ThibaultLemaire/SharpmonCore/tags
