namespace Sharpmon.FSharp

open Sharpmon.Interfaces
open System.IO
type 'a Persister(folder, serializeTo, deserializeFrom) =

    let saveFile saveName =
        let fileName = sprintf "%s.save.json" saveName 
        Path.Combine(folder, fileName)

    interface 'a IPersister with
        member __.Save (saveName, obj) =
            Directory.CreateDirectory folder |> ignore
            use stream =
                saveName
                |> saveFile
                |> File.CreateText
            obj |> serializeTo stream
        member __.Load saveName =
            use stream =
                saveName
                |> saveFile
                |> File.OpenText
            deserializeFrom stream
        member __.Has saveName =
            saveName
            |> saveFile
            |> File.Exists
        member __.Delete saveName =
            saveName
            |> saveFile
            |> File.Delete
    
    new(folder, serializer: IStreamSerializer<_>) =
        let serializeTo stream obj = serializer.SerializeTo(stream, obj)
        Persister(folder, serializeTo, serializer.DeserializeFrom)
