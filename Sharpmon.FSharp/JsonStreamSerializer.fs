namespace Sharpmon.FSharp

open Newtonsoft.Json;
open Sharpmon.Interfaces;
type JsonStreamSerializer<'a, 'b when 'b : struct>(deconstruct: 'a -> 'b, reconstruct: 'b -> 'a) =
    let serializer = JsonSerializer()
    do serializer.Formatting <- Formatting.Indented

    interface 'a IStreamSerializer with
        member __.SerializeTo (stream, object) =
            use writer = new JsonTextWriter(stream)
            serializer.Serialize(writer, deconstruct object)
        member __.DeserializeFrom stream =
            use reader = new JsonTextReader(stream)
            serializer.Deserialize<_>(reader) |> reconstruct
    
    new(factory: IFactory<_,_>) =
        JsonStreamSerializer(factory.Deconstruct, factory.Reconstruct)
