namespace Sharpmon.FSharp
open System.Text.RegularExpressions
open Sharpmon.Interfaces

module CLI =
    let formatInBoxes formatter lineLength =
        let formatLine (line: string) =
            line.PadRight lineLength
            |> sprintf "| %s |"
        
        let formatChunk index =
            formatter lineLength index
            >> List.map formatLine

        let separator =
            String.replicate (lineLength + 2) "-"
            |> sprintf "+%s+"
        
        let foldChunk state chunk = separator :: chunk @ state

        Seq.mapi formatChunk
        >> Seq.fold foldChunk [separator]
        >> String.concat "\n"

    let formatItem lineLength idx (item: IShopItem) =
        let index = idx + 1
        let spaces =
            let count =
                lineLength
                - item.Name.Length
                - (index |> string |> String.length)
                - 6
            
            " " |> String.replicate count

        let formattedDescription =
            let distributeWords (lines, currentLine, length) word =
                let wordLength = String.length word
                let newLength = length + 1 + wordLength
                if newLength <= lineLength then
                    (lines, word :: currentLine, newLength)
                else
                    (currentLine :: lines, [word], wordLength)
            
            let takeLastLine (lines, lastLine, _) =
                lastLine :: lines

            item.Description
            |> (Regex @"\s+").Split 
            |> Array.fold distributeWords ([], [], 0)
            |> takeLastLine
            |> List.map (List.rev >> String.concat " ")
            |> List.rev
        
        sprintf "%s %s id: %i" item.Name spaces index
        :: sprintf "Price: #%i" item.Price
        :: formattedDescription
    
    let itemsInBoxes items = items |> formatInBoxes formatItem 24
