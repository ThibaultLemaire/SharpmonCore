namespace Sharpmon.FSharp

open Sharpmon.Interfaces
[<System.Runtime.CompilerServices.Extension>]
module RandomRound =
    [<System.Runtime.CompilerServices.Extension>]
    let Round (random: IRandom) (value: double) =
        let intPart = int value
        match value % 1.0 with
        | 0.0 -> intPart
        | _ -> intPart + random.Next 2
