namespace Sharpmon.FSharp

open Sharpmon.Interfaces
type ShopItem (name, price, description) =
    interface IShopItem with
        member __.Name = name
        member __.Price = price
        member __.Description = description
