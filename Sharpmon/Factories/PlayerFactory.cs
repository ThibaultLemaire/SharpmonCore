using System.Collections.Generic;
using System.Linq;
using Sharpmon.Interfaces;

namespace Sharpmon.Factories
{
    public struct PlayerRepr
    {
        public readonly string Name;
        public readonly List<SharpmonRepr> Sharpmons;
        public readonly int CurrentSharpmon;
        public readonly int Money;
        public readonly Dictionary<string, int> Items;
        public readonly string Difficulty;

        public PlayerRepr(
            string name,
            List<SharpmonRepr> sharpmons,
            int currentSharpmon,
            int money,
            Dictionary<string, int> items,
            string difficulty
        )
        {
            Name = name;
            Sharpmons = sharpmons;
            CurrentSharpmon = currentSharpmon;
            Money = money;
            Items = items;
            Difficulty = difficulty;
        }
    }

    public class PlayerFactory : IFactory<Player, PlayerRepr>
    {
        IRandom random;
        IConsoleDisplay consoleDisplay;
        ItemByName itemFromName;
        IFactory<Sharpmon, SharpmonRepr> sharpmonFactory;

        public PlayerFactory(
            IRandom rand,
            IConsoleDisplay consoleDisplay,
            ItemByName itemFromName,
            IFactory<Sharpmon, SharpmonRepr> sharpmonFactory)
        {
            random = rand;
            this.consoleDisplay = consoleDisplay;
            this.itemFromName = itemFromName;
            this.sharpmonFactory = sharpmonFactory;
        }

        public PlayerRepr Deconstruct(Player subject)
        {
            return new PlayerRepr(
                subject.Name,
                subject.Sharpmons
                    .Select(sharpmonFactory.Deconstruct)
                    .ToList(),
                subject.Sharpmons.IndexOf(subject.CurrentSharpmon),
                subject.Money,
                subject.Items.ToDictionary(
                    x => x.Key,
                    x => x.Value.Count
                ),
                subject.Difficulty
            );
        }

        public Player Reconstruct(PlayerRepr material)
        {
            var sharpmons = material.Sharpmons
                                    .Select(sharpmonFactory.Reconstruct)
                                    .ToList();
            return new Player(
                material.Name,
                sharpmons,
                sharpmons[material.CurrentSharpmon],
                material.Money,
                material.Items.ToDictionary(
                    x => x.Key,
                    x =>
                    {
                        Item item = itemFromName(x.Key);
                        item.Count = x.Value;
                        return item;
                    }
                ),
                material.Difficulty,
                random,
                consoleDisplay
            );
        }
    }
}
