using System.Collections.Generic;
using System.Linq;
using Sharpmon.Interfaces;

namespace Sharpmon.Factories
{
    public struct SharpmonRepr
    {
        public readonly string Name;
        public readonly int Level;
        public readonly int XP;
        public readonly int XPToNextLevel;
        public readonly int MaxHP;
        public readonly int HP;
        public readonly int BasePower;
        public readonly int Power;
        public readonly int BaseDefense;
        public readonly int Defense;
        public readonly int BaseDodge;
        public readonly int Dodge;
        public readonly int BaseAccuracy;
        public readonly int Accuracy;
        public readonly int BaseSpeed;
        public readonly int Speed;
        public readonly IEnumerable<string> Attacks;

        public SharpmonRepr(
            string name,
            int level,
            int xp,
            int xpToNextLevel,
            int maxHp,
            int hp,
            int basePower,
            int power,
            int baseDefense,
            int defense,
            int baseDodge,
            int dodge,
            int baseAccuracy,
            int accuracy,
            int baseSpeed,
            int speed,
            IEnumerable<string> attacks
        )
        {
            Name = name;
            Level = level;
            XP = xp;
            XPToNextLevel = xpToNextLevel;
            MaxHP = maxHp;
            HP = hp;
            BasePower = basePower;
            Power = power;
            BaseDefense = baseDefense;
            Defense = defense;
            BaseDodge = baseDodge;
            Dodge = dodge;
            BaseAccuracy = baseAccuracy;
            Accuracy = accuracy;
            BaseSpeed = baseSpeed;
            Speed = speed;
            Attacks = attacks;
        }
    }

    public class SharpmonFactory : IFactory<Sharpmon, SharpmonRepr>
    {
        readonly IConsoleDisplay consoleDisplay;
        readonly IRandom random;
        Register<Attack> attacks;

        public SharpmonFactory(
            IConsoleDisplay consoleDisplay,
            IRandom random,
            Register<Attack> attacks)
        {
            this.attacks = attacks;
            this.consoleDisplay = consoleDisplay;
            this.random = random;
        }

        public SharpmonRepr Deconstruct(Sharpmon subject)
        {
            return new SharpmonRepr(
                subject.Name,
                subject.Level,
                subject.XP,
                subject.XPToNextLevel,
                subject.MaxHP,
                subject.HP,
                subject.BasePower,
                subject.Power,
                subject.BaseDefense,
                subject.Defense,
                subject.BaseDodge,
                subject.Dodge,
                subject.BaseAccuracy,
                subject.Accuracy,
                subject.BaseSpeed,
                subject.Speed,
                subject.Attacks
                    .Select(x => x.Name)
            );
        }

        public Sharpmon Reconstruct(SharpmonRepr material)
        {
            return new Sharpmon(
                material.Name,
                material.Level,
                material.XP,
                material.XPToNextLevel,
                material.MaxHP,
                material.HP,
                material.BasePower,
                material.Power,
                material.BaseDefense,
                material.Defense,
                material.BaseDodge,
                material.Dodge,
                material.BaseAccuracy,
                material.Accuracy,
                material.BaseSpeed,
                material.Speed,
                material.Attacks
                    .Select(x => attacks[x])
                    .ToList(),
                random,
                consoleDisplay
            );
        }
    }
}
