using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Sharpmon.Interfaces;

namespace Sharpmon
{
    class ConsoleEventDisplayer
    {
        readonly IConsoleDisplay console;

        public ConsoleEventDisplayer(
            IConsoleDisplay console
        )
        {
            this.console = console;
        }

        public void OnSharpmonStatIncreased(
            Sharpmon subject,
            Expression<Func<Sharpmon, int>> statPicker,
            int amount
        )
        {
            var me = (MemberExpression)statPicker.Body;
            var statName = me.Member.Name;
            console.WriteLine(
                StatColor(statName),
                $"{subject.Name}'s {statName} permanently increased by {amount}.");
        }

        public void OnSharpmonLeveledUp(Sharpmon subject)
        {
            console.WriteLine(
                ConsoleColor.Cyan,
                $"{subject.Name} levels up!\n{subject.Name} is now level {subject.Level}"
            );
        }

        public void OnSharpmonHealed(
            Sharpmon subject,
            int amount
        )
        {
            console.WriteLine(
                ConsoleColor.Green,
                $"{subject.Name} is restored {amount} HP"
            );
        }

        ConsoleColor StatColor(string statName)
        {
            switch (statName)
            {
                case nameof(Sharpmon.Accuracy):
                    return ConsoleColor.DarkYellow;
                case nameof(Sharpmon.Defense):
                    return ConsoleColor.DarkGreen;
                case nameof(Sharpmon.Dodge):
                    return ConsoleColor.DarkMagenta;
                case nameof(Sharpmon.Power):
                    return ConsoleColor.DarkRed;
                case nameof(Sharpmon.Speed):
                    return ConsoleColor.DarkCyan;
                default:
                    return ConsoleColor.White;
            }
        }
    }
}
