using System;
using Sharpmon.Interfaces;

namespace Sharpmon
{
    class ConsoleDisplay : IConsoleDisplay
    {
        public string Title
        {
            set => Console.Title = value;
        }

        public bool KeyAvailable => Console.KeyAvailable;

        public ConsoleKeyInfo ReadKey(bool intercept)
            => Console.ReadKey(intercept);
        public ConsoleKeyInfo ReadKey() => Console.ReadKey();

        public string ReadLine() => Console.ReadLine();

        public void Write(string value) => Console.Write(value);

        public void WriteLine() => Console.WriteLine();
        public void WriteLine(string s) => Console.WriteLine(s);
        public void WriteLine<T>(string format, T t)
            => Console.WriteLine(format, t);
        public void WriteLine<T, U>(string format, T t, U u)
            => Console.WriteLine(format, t, u);
        public void WriteLine(ConsoleColor color, string s)
            => UsingColor(color, () => Console.WriteLine(s));
        public void WriteLine<T>(ConsoleColor color, string format, T t)
            => UsingColor(color, () => Console.WriteLine(format, t));
        public void WriteLine<T, U>(
            ConsoleColor color, string format, T t, U u)
            => UsingColor(color, () => Console.WriteLine(format, t, u));
        public void WriteLine<T, U, V>(
            ConsoleColor color, string format, T t, U u, V v)
            => UsingColor(color, () => Console.WriteLine(format, t, u, v));

        public void Clear() => Console.Clear();

        void UsingColor(ConsoleColor color, Action act)
        {
            Console.ForegroundColor = color;
            act();
            Console.ResetColor();
        }
    }
}