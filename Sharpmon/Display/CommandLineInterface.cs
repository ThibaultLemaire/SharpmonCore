﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sharpmon.Interfaces;
using Sharpmon.FSharp;

namespace Sharpmon
{
    public class CommandLineInterface
    {
        IConsoleDisplay Console;
        readonly string appName;
        readonly Version version;
        readonly IRandom random;
        IPersister<Player> persister;
        readonly Register<Sharpmon> sharpDex;
        readonly NewPlayerBuilder CreatePlayer;
        readonly List<IShopItem> availableItems;
        readonly ItemByName itemByName;

        Player Trainer;

        public static string Difficulty;


        public CommandLineInterface(
            IConsoleDisplay consoleDisplay,
            string appName,
            Version version,
            IRandom random,
            IPersister<Player> prstr,
            Register<Sharpmon> sharpDex,
            List<IShopItem> availableItems,
            ItemByName itemByName,
            NewPlayerBuilder createPlayer)
        {
            Console = consoleDisplay;
            this.appName = appName;
            this.version = version;
            this.random = random;
            persister = prstr;
            this.sharpDex = sharpDex;
            this.availableItems = availableItems;
            CreatePlayer = createPlayer;
            this.itemByName = itemByName;
        }

        void ChangeTitle(string title)
        {
            Console.Title = title + " (" + Difficulty + ")";
        }

        int MenuCore(int maxAnswer = 0, string instructions = "", int minAnswer = 0, bool justPause = false)
        {

            int choice = 0;
            if (maxAnswer == 0 && justPause)
            {
                while (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                }
                if (instructions == "")
                {
                    instructions = "Press any key";
                }
                Console.Write(instructions);
                Console.ReadKey();
                Console.WriteLine();
            }
            else
            {
                if (instructions == "")
                {
                    instructions = "Please enter a number" + (maxAnswer == 0 ? "" : " between 0 and " + maxAnswer) + ": ";
                }
                bool choiceParsableToInt = false;
                while (choice < minAnswer || (maxAnswer == 0 ? false : choice > maxAnswer) || !choiceParsableToInt)
                {
                    Console.Write(instructions);
                    string input = Console.ReadLine();
                    switch (input)
                    {
                        // cheatcodes
                        case "hellomynameis":
                            if (Trainer != null)
                            {
                                string newName = "";

                                while (newName == "")
                                {
                                    Console.WriteLine("Enter your name:");
                                    newName = Console.ReadLine();
                                }

                                persister.Delete(Trainer.Name);

                                Trainer.Name = newName;

                                Console.WriteLine(ConsoleColor.White, "Welcome to Sharpmon world, {0}!", Trainer.Name);
                            }
                            break;
                        case "gimmemoney":
                            if (Trainer != null)
                            {
                                Trainer.Money += 1000;
                                Console.WriteLine(ConsoleColor.Yellow, "You have #{0}", Trainer.Money);
                            }
                            break;
                        case "lettherebelight":
                            if (Trainer != null)
                            {
                                Trainer.CurrentSharpmon.HP = Trainer.CurrentSharpmon.MaxHP;
                                Console.WriteLine(ConsoleColor.Green, "{0} is back to full health.", Trainer.CurrentSharpmon);
                            }
                            break;
                        case "areyouontheball":
                            if (Trainer != null)
                            {
                                Trainer.Items["Sharpball"].Count++;
                                Console.WriteLine(ConsoleColor.Cyan, "You draw a sharpball out of your pocket.");
                            }
                            break;
                    }
                    choiceParsableToInt = int.TryParse(input, out choice);
                }
            }

            return choice;
        }

        T Menu<T>(string title, List<string> options)
        {
            options.RemoveAll(x => x == "");
            int i = -1;

            string text = "\n" + title;

            foreach (string option in options)
            {
                text += "\n\t" + ++i + " : " + option;
            }

            Console.WriteLine(text);

            int choice = MenuCore(i);

            if (typeof(T) == typeof(int))
            {
                return (T)Convert.ChangeType(choice, typeof(T));
            }
            else if (typeof(T) == typeof(string))
            {
                return (T)Convert.ChangeType(options[choice], typeof(T));
            }
            else
            {
                throw new NotImplementedException(); // au cas où quelqu'un appellerait Menu() avec un autre type que int ou string
            }
        }

        Sharpmon GetRandomSharpmon()
        {
            List<string> sharpmonNames = sharpDex.Keys.ToList();

            return new Sharpmon(sharpDex[sharpmonNames[random.Next(sharpmonNames.Count)]]);
        }

        public List<Sharpmon> DisplaySharpmons(Player player, out string text, bool displayCurrent = true, bool displayKO = true)
        {
            int id = 0;
            text = "\n";
            var displayedSharpmons = new List<Sharpmon>();
            var currentSharpmon = player.CurrentSharpmon;
            var sharpmons = player.Sharpmons;
            if (displayCurrent)
            {
                id = 1;
                var hasOnlyOneSharpmon = sharpmons.Count == 1;
                var HPRatio = currentSharpmon.HP + "/" + currentSharpmon.MaxHP;
                var XPRatio = currentSharpmon.XP + "/" + currentSharpmon.XPToNextLevel;
                text += "+------------------------+";
                text += "\n|+----------------------+|\n"
                    + "|| " + currentSharpmon + (currentSharpmon.Name.Count() < 5 ? "\t" : "") + (currentSharpmon.Name.Count() < 13 ? "\t" : " ") + "id: 1"
                    + (currentSharpmon.Name.Count() < 15 ? "\t" : "") + (currentSharpmon.Name.Count() < 16 ? "|" : "") + (currentSharpmon.Name.Count() < 17 ? "|" : "") + "\n"
                    + "|| HP: " + HPRatio + (HPRatio.Count() < 9 ? "\t" : "") + "\t||\n"
                    + "|| XP: " + XPRatio + (XPRatio.Count() < 9 ? "\t" : "") + "\t||\n"
                    + "|| Lvl: " + currentSharpmon.Level + "\t" + "Spd: " + currentSharpmon.BaseSpeed + "\t||\n"
                    + "|| Pow: " + currentSharpmon.BasePower + "\t" + "Def: " + currentSharpmon.BaseDefense + "\t||\n"
                    + "|| Acc: " + currentSharpmon.BaseAccuracy + "\t" + "Dod: " + currentSharpmon.BaseDodge + "\t||\n"
                    + "|+----------------------+|\n"
                    + "+" + (hasOnlyOneSharpmon ? "-" : "+") + "----------------------" + (hasOnlyOneSharpmon ? "-" : "+") + "+";

                displayedSharpmons.Add(currentSharpmon);
            }
            else text += " +----------------------+"; ;


            foreach (Sharpmon sharpmon in sharpmons)
            {
                if (sharpmon != currentSharpmon && (sharpmon.HP > 0 || displayKO))
                {
                    var HPRatio = sharpmon.HP + "/" + sharpmon.MaxHP;
                    var XPRatio = sharpmon.XP + "/" + sharpmon.XPToNextLevel;

                    text += "\n | " + sharpmon + (sharpmon.Name.Count() < 5 ? "\t" : "") + (sharpmon.Name.Count() < 13 ? "\t" : " ") + "id: " + ++id
                        + (sharpmon.Name.Count() < 15 ? "\t" : "") + (sharpmon.Name.Count() < 16 ? "|" : "") + "\n"
                        + " | HP: " + HPRatio + (HPRatio.Count() < 9 ? "\t" : "") + "\t|\n"
                        + " | XP: " + XPRatio + (XPRatio.Count() < 9 ? "\t" : "") + "\t|\n"
                        + " | Lvl: " + sharpmon.Level + "\tSpd: " + sharpmon.BaseSpeed + "\t|\n"
                        + " | Pow: " + sharpmon.BasePower + "\tDef: " + sharpmon.BaseDefense + "\t|\n"
                        + " | Acc: " + sharpmon.BaseAccuracy + "\tDod: " + sharpmon.BaseDodge + "\t|\n"
                        + " +----------------------+";

                    displayedSharpmons.Add(sharpmon);
                }
            }

            return displayedSharpmons;
        }

        bool SwitchSharpmon(Player player, bool lastChance = false)
        {
            List<Sharpmon> availableSharpmons = DisplaySharpmons(player, out string text, displayCurrent: false, displayKO: false);

            if (lastChance && player.GetAliveSharpmonsCount() == 1)
            {
                player.SwitchCurrentSharpmon(availableSharpmons[0]);
            }
            else
            {
                Console.WriteLine(text);

                if (lastChance)
                {
                    player.SwitchCurrentSharpmon(availableSharpmons[MenuCore(availableSharpmons.Count, "Choose a Sharpmon (between 1 and " + availableSharpmons.Count + "): ", 1) - 1]);
                }
                else
                {
                    int NewSharpmonIndex = MenuCore(availableSharpmons.Count, "Choose a Sharpmon (" + (availableSharpmons.Count == 1 ? "1 or" : "between 1 and " + availableSharpmons.Count + ",") + " 0 to Cancel): ");

                    switch (NewSharpmonIndex)
                    {
                        case 0:
                            return false;
                        default:
                            player.SwitchCurrentSharpmon(availableSharpmons[NewSharpmonIndex - 1]);
                            break;
                    }
                }
            }
            return true;
        }

        bool UseItem(Player player)
        {
            var options = new string[] { "Cancel" }
                            .Concat(player.Items.Keys
                                .Where(x => x != "Sharpball"))
                            .ToList();

            var choice = Menu<string>("Select item to use :", options);

            if (choice == "Cancel")
                return false;

            Sharpmon target = player.CurrentSharpmon;
            if (player.Sharpmons.Count > 1)
            {
                var allSharpmons = DisplaySharpmons(player, out string text);
                Console.WriteLine(text);

                var sharpmonOptions = (allSharpmons.Count == 1 ? "1 or" : $"between 1 and {allSharpmons.Count},");
                var sharpmonIndex = MenuCore(
                    allSharpmons.Count, $"Choose a Sharpmon ({sharpmonOptions} 0 to Cancel): ");

                if (sharpmonIndex == 0)
                    return false;

                target = allSharpmons[sharpmonIndex - 1];
            }

            var selectedItem = player.Items[choice];

            selectedItem.Count--;
            if (selectedItem.Count == 0)
                player.Items.Remove(choice);

            Console.WriteLine(ConsoleColor.White, "You used {0} on {1}", selectedItem, target);

            selectedItem.Activate(target);

            return true;
        }

        void Turn(Player trainer, Sharpmon opponent, out bool endFight)
        {
            endFight = false;
            bool exitLoop = false;
            while (!exitLoop)
            {
                switch (Menu<string>("Your turn.", new List<string>
                {
                    (trainer.CurrentSharpmon.HP == 0 ? "" : "Attack"),
                    (trainer.Sharpmons.Count > 1 && trainer.GetAliveSharpmonsCount() > 1 ? "Switch Sharpmon" : ""),
                    (trainer.Items.Count > 1 ? "Use Item" : ""),
                    (trainer.Items["Sharpball"].Count > 0 ? "Capture" : ""),
                    "Run Away"
                }))
                {
                    case "Attack":
                        List<string> attackOptions = trainer.CurrentSharpmon.Attacks.ConvertAll(x => x.ToString());
                        attackOptions.Add("Go back to previous menu");


                        int choice = Menu<int>("Choose an attack for " + trainer.CurrentSharpmon, attackOptions);
                        if (choice < attackOptions.Count - 1)
                        {
                            if (!trainer.CurrentSharpmon.Attacks[choice].Activate(trainer.CurrentSharpmon, opponent))
                            {
                                Console.WriteLine(ConsoleColor.Blue, "Nothing happens");
                            }
                            exitLoop = true;
                        }
                        break;
                    case "Switch Sharpmon":
                        exitLoop = SwitchSharpmon(trainer);
                        break;
                    case "Use Item":
                        exitLoop = UseItem(trainer);
                        break;
                    case "Capture":
                        if (trainer.Capture(opponent))
                        {
                            endFight = true;
                            return;
                        }
                        exitLoop = true;
                        break;
                    case "Run Away":
                        if (trainer.RunAwayFrom(opponent))
                        {
                            Console.WriteLine(ConsoleColor.Cyan, "You managed to escape from the fight.");

                            endFight = true;
                            return;
                        }
                        else
                        {
                            Console.WriteLine(ConsoleColor.White, "You failed to escape from the fight.");
                        }
                        exitLoop = true;
                        break;
                }
            }
        }

        void Battle()
        {
            Console.Clear();

            Sharpmon wildSharpmon = GetRandomSharpmon();

            Sharpmon leader = Trainer.CurrentSharpmon; // Sauvegarde du leader actuel du joueur pour pouvoir le reset dans currentSharpmon à la fin du combat

            ChangeTitle("In fight with a wild " + wildSharpmon);
            Console.WriteLine("You encounter a wild {0}", wildSharpmon);
            Console.WriteLine(ConsoleColor.White, "Let the battle begin!");

            while (wildSharpmon.Level < Trainer.CurrentSharpmon.Level)
            {
                wildSharpmon.LevelUp();
            }
            
            wildSharpmon.Reset();
            foreach (Sharpmon sharpmon in Trainer.Sharpmons)
            {
                sharpmon.Reset();
            }

            Dictionary<Sharpmon, int> SharpmonPlayedTurnsMap = new Dictionary<Sharpmon, int>(); // sert à enregistrer tous les sharpmons ayant participé au combat pour la répartition de l'xp

            int tokenCostforTurn = 10;
            int firstToken = random.Next(2); // on donne un jeton au hasard au joueur ou à l'ennemi
            int playerTakeTurnTokens = firstToken;
            int enemyTakeTurnTokens = 1 - firstToken;
            // voir plus bas pour le fonctionnement des jetons de tour

            while (true)
            {
                string dashes = "+-----------------------+";
                string text = dashes + "\n"
                    + "| Enemy " + wildSharpmon + (wildSharpmon.Name.Count() < 8 ? "\t" : "") + "\t|\n"
                    + "| HP: " + wildSharpmon.HP + "/" + wildSharpmon.MaxHP + "\tLvl: " + wildSharpmon.Level + "\t|\n"
                    + "| Pow: " + wildSharpmon.Power + "\t" + "Def: " + wildSharpmon.Defense + "\t|\n"
                    + "| Acc: " + wildSharpmon.Accuracy + "\t" + "Dod: " + wildSharpmon.Dodge + "\t|\n"
                    + dashes + "\n"
                    + "| Your " + Trainer.CurrentSharpmon + (Trainer.CurrentSharpmon.Name.Count() < 9 ? "\t" : "") + (Trainer.CurrentSharpmon.Name.Count() < 17 ? "\t" : "")
                    + (Trainer.CurrentSharpmon.Name.Count() < 18 ? "|" : "") + "\n"
                    + "| HP: " + Trainer.CurrentSharpmon.HP + "/" + Trainer.CurrentSharpmon.MaxHP + "\tLvl: " + Trainer.CurrentSharpmon.Level + "\t|\n"
                    + "| Pow: " + Trainer.CurrentSharpmon.Power + "\t" + "Def: " + Trainer.CurrentSharpmon.Defense + "\t|\n"
                    + "| Acc: " + Trainer.CurrentSharpmon.Accuracy + "\t" + "Dod: " + Trainer.CurrentSharpmon.Dodge + "\t|\n"
                    + dashes;
                Console.WriteLine(text);

                while (playerTakeTurnTokens < tokenCostforTurn && enemyTakeTurnTokens < tokenCostforTurn)
                {
                    // TAKE TURN TOKENS
                    // Un sharpmon ayant plus de vitesse que son adversaire peut finir par jouer deux fois
                    // avant de passer la main
                    playerTakeTurnTokens += Trainer.CurrentSharpmon.Speed;
                    enemyTakeTurnTokens += wildSharpmon.Speed;
                    if (playerTakeTurnTokens == tokenCostforTurn && enemyTakeTurnTokens == tokenCostforTurn)
                    {
                        playerTakeTurnTokens = 0;
                        enemyTakeTurnTokens = 0;
                    }
                }

                if (enemyTakeTurnTokens < playerTakeTurnTokens) // tour du joueur
                {
                    if (SharpmonPlayedTurnsMap.ContainsKey(Trainer.CurrentSharpmon)) // enregistrement des sharpmons ayant participé
                    {
                        SharpmonPlayedTurnsMap[Trainer.CurrentSharpmon]++;
                    }
                    else
                    {
                        SharpmonPlayedTurnsMap.Add(Trainer.CurrentSharpmon, 1);
                    }

                    Turn(Trainer, wildSharpmon, out bool endFight);
                    if (endFight) break;

                    playerTakeTurnTokens -= tokenCostforTurn;
                }
                else // tour de l'adversaire
                {
                    Console.WriteLine(ConsoleColor.White, "{0}'s turn", wildSharpmon);
                    if (!wildSharpmon.Attacks[random.Next(wildSharpmon.Attacks.Count)].Activate(wildSharpmon, Trainer.CurrentSharpmon))
                    {
                        Console.WriteLine(ConsoleColor.Blue, "Nothing happens");
                    }
                    enemyTakeTurnTokens -= tokenCostforTurn;
                }

                if (Trainer.CurrentSharpmon.HP == 0)
                {
                    if (Difficulty == "Hardcore")
                    {
                        Trainer.Sharpmons.Remove(Trainer.CurrentSharpmon);
                    }
                    if (Trainer.GetAliveSharpmonsCount() == 0) // Défaite
                    {
                        if (Difficulty == "Hardcore")
                        {
                            Console.Title = "GAME OVER";
                            Console.WriteLine(ConsoleColor.Red, "This was your last Sharpmon.\nGAME OVER");
                            persister.Delete(Trainer.Name);
                            System.Threading.Thread.Sleep(3000);
                            MenuCore(justPause: true);
                            Environment.Exit(0);
                        }
                        else
                        {
                            Console.WriteLine(ConsoleColor.Red, "You lost this fight.");
                        }
                        
                        break;
                    }
                    else
                    {
                        SwitchSharpmon(Trainer, lastChance: true);
                        playerTakeTurnTokens = 0;
                        enemyTakeTurnTokens += 1;
                    }
                    
                }

                if (wildSharpmon.HP == 0) // Victoire
                    {
                        Console.WriteLine(ConsoleColor.Green, "You win!");

                        #region XP Repartition

                        foreach (Sharpmon sharpmon in SharpmonPlayedTurnsMap.Keys.ToList())
                        {
                            if (sharpmon.HP == 0)
                            {
                                SharpmonPlayedTurnsMap.Remove(sharpmon); // On retire les sharpmons KO
                            }
                        }

                        int totalxp = random.Next(100, 500) * wildSharpmon.Level;
                        int totalTurns = 0; // nombre total de tours joués par les Sharpmons du Player
                        foreach (int turnCount in SharpmonPlayedTurnsMap.Values.ToList())
                        {
                            totalTurns += turnCount;
                        }
                        int xpToGive = totalxp / totalTurns; // parts d'xp à donner à chaque sharpmon
                        // (Il est très possible que ça ne tombe pas rond et que (xpToGive * totalTurns) ne soit pas égal à totalxp)

                        foreach (Sharpmon sharpmon in SharpmonPlayedTurnsMap.Keys.ToList())
                        {
                            // On donne à chaque sharpmon le même nombre de parts d'xp que le nombre de tours qu'il est resté dans le combat.
                            // L'expression ternaire de la ligne suivante assure que toute l'xp a bien été distribuée.
                            int xpEarned = xpToGive * SharpmonPlayedTurnsMap[sharpmon] + (sharpmon == Trainer.CurrentSharpmon ? totalxp - xpToGive * totalTurns : 0);

                            int previousLvl = sharpmon.Level;

                            sharpmon.XP += xpEarned;

                            Console.WriteLine(ConsoleColor.Magenta, "{0} earns {1} XP", sharpmon, xpEarned);
                            if (previousLvl < sharpmon.Level)
                            {
                                Console.WriteLine(ConsoleColor.Cyan, "{0} levels up!\n{0} is now level {1}", sharpmon, sharpmon.Level);
                            }
                        }
                        #endregion

                        int money = random.Next(100, 500);
                        Trainer.Money += money;
                        Console.WriteLine(ConsoleColor.Yellow, "You are rewarded #{0}", money);

                        break;
                    }
            }

            System.Threading.Thread.Sleep(1000);
            MenuCore(justPause: true);
            Console.Clear();

            if (leader.HP > 0)
            {
                Trainer.CurrentSharpmon = leader;
            }
        }

        void SharpmonCenter()
        {
            Console.Clear();
            ChangeTitle("Sharpmon Center");
            Console.WriteLine(ConsoleColor.White, "======SHARPMON===CENTER======");

            bool exitLoop = false;

            while (!exitLoop)
            {
                string text;
                List<Sharpmon> allSharpmons = DisplaySharpmons(Trainer, out text);
                Console.WriteLine(text);

                bool someSharpmonHurt = false;
                int sharpmonsKO = 0;

                foreach (Sharpmon sharpmon in Trainer.Sharpmons)
                {
                    if (sharpmon.HP < sharpmon.MaxHP)
                    {
                        someSharpmonHurt = true;
                    }
                    if (sharpmon.HP == 0 && sharpmon != Trainer.CurrentSharpmon)
                    {
                        sharpmonsKO++;
                    }
                }

                switch (Menu<string>("Welcome to the Sharpmon Center!", new List<string>
                    {
                        (someSharpmonHurt ? "Heal all" : ""),
                        (Trainer.Sharpmons.Count > 1 && someSharpmonHurt ? "Heal" : ""),
                        (Trainer.Sharpmons.Count > 1 && sharpmonsKO < allSharpmons.Count - 1 ? "Select leader" : ""),
                        "Rename",
                        (Trainer.Items.Count > 1 ? "Use Item" : ""),
                        "Leave",
                    }))
                {
                    case "Heal all":
                        foreach (Sharpmon sharpmon in Trainer.Sharpmons)
                        {
                            sharpmon.HP = sharpmon.MaxHP;
                        }
                        Console.WriteLine(ConsoleColor.Green, "All your Sharpmons are back to full health.");
                        break;
                    case "Heal":
                        int sharpmonIndex = MenuCore(allSharpmons.Count, "Choose a Sharpmon (" + (allSharpmons.Count == 1? "1 or" : "between 1 and " + allSharpmons.Count + ",") + " 0 to Cancel): ");

                        if (sharpmonIndex != 0)
                        {
                            Sharpmon sharpmonToHeal = allSharpmons[sharpmonIndex - 1];
                            sharpmonToHeal.HP = sharpmonToHeal.MaxHP;
                            Console.WriteLine(ConsoleColor.Green, "{0} is back to full health.", sharpmonToHeal);
                        }
                        break;
                    case "Select leader":
                        SwitchSharpmon(Trainer);
                        break;
                    case "Rename":
                        int sharpIndex = MenuCore(allSharpmons.Count, "Choose a Sharpmon (" + (allSharpmons.Count == 1 ? "1 or" : "between 1 and " + allSharpmons.Count + ",") + " 0 to Cancel): ");

                        if (sharpIndex != 0)
                        {
                            allSharpmons[sharpIndex - 1].Rename();
                        }
                        break;
                    case "Use Item":
                        UseItem(Trainer);
                        break;
                    case "Leave":
                        exitLoop = true;
                        break;
                }
            }
            Console.Clear();
        }

        public void BuyItem(Player player, IShopItem item, int quantity)
        {
            if (quantity == 0)
            {
                Console.WriteLine("You didn't buy anything.");
                return;
            }

            var money = player.Money;
            if (item.Price > money)
            {
                Console.WriteLine(ConsoleColor.Red, "Not enough Money");
                return;
            }

            var buyableQuantity = money / item.Price;
            var boughtQuantity = (quantity < buyableQuantity ? quantity : buyableQuantity);
            var items = player.Items;

            if (!items.ContainsKey(item.Name))
            {
                items.Add(item.Name, itemByName(item.Name));
            }

            items[item.Name].Count += boughtQuantity;

            player.Money -= item.Price * boughtQuantity;
            Console.WriteLine(ConsoleColor.DarkYellow, "You bought {0} {1}", boughtQuantity, item.Name);
        }

        /// <remarks>
        /// Expects item to be in player's inventory
        /// </remarks>
        public void SellItem(Player player, Item item, int quantity = 1)
        {
            if (quantity > 0)
            {
                if (quantity < item.Count)
                {
                    player.Money += quantity * item.SellPrice;
                    item.Count -= quantity;
                    Console.WriteLine(ConsoleColor.Yellow, "You sold {0} {1}", quantity, item);
                }
                else if (item.Name == "Sharpball")
                {
                    player.Money += (item.Count - 1) * item.SellPrice;
                    Console.WriteLine(ConsoleColor.Yellow, "You sold {0} {1}", item.Count - 1, item);
                    item.Count = 1;
                }
                else
                {
                    player.Money += item.Count * item.SellPrice;
                    Console.WriteLine(ConsoleColor.Yellow, "You sold {0} {1}", item.Count, item);
                    player.Items.Remove(item.Name);
                }
            }
            else Console.WriteLine("You didn't sell anything.");
        }

        void Shop()
        {
            Console.Clear();
            ChangeTitle("Shop");
            Console.WriteLine(ConsoleColor.White, "============SHOP=============");

            Console.WriteLine("Welcome to the Shop!");

            if (Trainer.Items["Sharpball"].Count == 0)
            {
                Trainer.Items["Sharpball"].Count = 1;
                Console.WriteLine(ConsoleColor.White, "Looks like you don't have any Sharpball left...\nHere's one for free!");
            }

            bool exitLoop = false;
            string dashes = "+-----------------------+";

            var itemsDisplay = CLI.itemsInBoxes(availableItems);

            while (!exitLoop)
            {
                Console.WriteLine(ConsoleColor.Yellow, "You have #{0}", Trainer.Money);

                switch (Menu<string>("What would you like to do?", new List<string>
                {
                    "Buy",
                    (Trainer.Items.Count > 1 || Trainer.Items["Sharpball"].Count > 1 ? "Sell" : ""),
                    "Leave",
                }))
                {
                    case "Buy":
                        Console.WriteLine(itemsDisplay);

                        var itemCount = availableItems.Count;
                        var itemOptions = (itemCount == 1 ? "1 or" : $"between 1 and {itemCount},");
                        var index = MenuCore(
                            itemCount, $"Choose an item ({itemOptions} 0 to cancel): ");

                        if (index != 0)
                        {
                            var quantity = MenuCore(instructions: "How many?\n");
                            BuyItem(Trainer, availableItems[index - 1], quantity);
                        }
                        break;

                    case "Sell":
                        int salableItemsCount = 0;
                        string salableItemsText = dashes;
                        List<Item> salableItems = Trainer.Items.Values.ToList();
                        if (Trainer.Items["Sharpball"].Count < 2)
                        {
                            salableItems.RemoveAt(0);
                        }

                        foreach (Item item in salableItems)
                        {
                            salableItemsText += "\n| " + item + "\tid: " + ++salableItemsCount + "\t|\n"
                            + "| Price: #" + item.SellPrice + "\t\t|\n"
                            + "| Quantity: " + item.Count + "\t\t|\n"
                            + dashes;
                        }

                        Console.WriteLine(salableItemsText);
                        int salableItemIndex = MenuCore(salableItemsCount, "Choose an item (" + (salableItemsCount == 1 ? "1 or" : "between 1 and " + salableItemsCount + ",") + " 0 to cancel): ");

                        if (salableItemIndex != 0)
                        {
                            int quantity = MenuCore(instructions: "How many?\n");
                            SellItem(Trainer, salableItems[salableItemIndex - 1], quantity);
                        }
                        break;

                    case "Leave":
                        exitLoop = true;
                        break;
                }
            }
            Console.Clear();
        }
        
        Player NewPlayer(string name)
        {
            Console.WriteLine(ConsoleColor.White, "Welcome to Sharpmon world, {0}!", name);

            Sharpmon firstSharpmon = new Sharpmon(sharpDex[Menu<string>(
                        "Choose your first Sharpmon, " + name + " :",
                        new List<string>
                        {
                                        "Sharpmander",
                                        "Sharpitle",
                                        "Sharpasaur"
                        })]);


            Console.WriteLine("Your new {0} is pleased to have you as its trainer, {1}!", firstSharpmon, name);


            Difficulty = Menu<string>(
                        "Choose a difficulty :",
                        new List<string>
                        {
                                        "Normal",
                                        "Hardcore"
                        });

            var items = new Dictionary<string, Item>() { { "Sharpball", itemByName("Sharpball") } };
            items["Sharpball"].Count = 1;
            return CreatePlayer(name, firstSharpmon, items, Difficulty);
        }

        public void Start()
        {
            Console.Title = appName;
            Console.WriteLine($"== {appName} v{version} ==");

            Console.WriteLine("Hello Stranger, what's your name?");
            string Name = Console.ReadLine();
            if (Name == "")
            {
                Name = "Sacha";
                Console.WriteLine("Don't wanna say your name? I'll call you {0}!", Name);
            }

            if (persister.Has(Name))
            {
                switch (Menu<int>("I've met a " + Name + " before...\nAre you the same " + Name + "?", new List<string>
                {
                    "Yes, it's me! (Keep playing as " + Name +")",
                    "I don't think so. (Overwrite previous profile named " + Name +")"

                }))
                {
                    case 0:
                        Trainer = persister.Load(Name);

                        Console.WriteLine(ConsoleColor.White, "Welcome back to Sharpmon world, {0}!", Trainer.Name);
                        break;

                    case 1:
                        Trainer = NewPlayer(Name);
                        break;
                }
            }
            else Trainer = NewPlayer(Name);

            Difficulty = Trainer.Difficulty;
            
            Console.Clear();
            while (true)
            {
                ChangeTitle("Main Menu");

                switch (Menu<string>(
                "Now, " + Trainer + ", where do you want to go?",
                new List<string>
                {
                    (Trainer.CurrentSharpmon.HP == 0 ? "" : "Into the Wild!"),
                    "To the Shop",
                    "To the Sharpmon Center",
                    "Away from here! (Exit game)"
                }))
                {
                    case "Into the Wild!":
                        Battle();
                        break;
                    case "To the Shop":
                        Shop();
                        break;
                    case "To the Sharpmon Center":
                        SharpmonCenter();
                        break;
                    case "Away from here! (Exit game)":
                        persister.Save(Trainer.Name, Trainer);
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}
