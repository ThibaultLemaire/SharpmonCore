using Ninject.Modules;
using System;
using System.IO;
using Sharpmon.Interfaces;

namespace Sharpmon.DI.Modules
{
    public class MiscModule : NinjectModule
    {
        public override void Load()
        {
            var appName = nameof(Sharpmon);
            var homeFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var appFolderName = $".{appName.ToLower()}";
            var appFolder = Path.Combine(homeFolder, appFolderName);
            var savesFolder = Path.Combine(appFolder, "Saves");

            Bind<string>().ToConstant(appName).Named("AppName");
            Bind<string>().ToConstant(savesFolder).WhenInjectedInto<IPersister<Player>>();
        }
    }
}
