using System.Collections.Generic;
using Ninject;
using Ninject.Modules;
using Sharpmon.Interfaces;

namespace Sharpmon.DI.Modules
{
    public class SharpdexModule : NinjectModule
    {
        public override void Load()
        {
            var attackDex = Kernel.Get<Register<Attack>>();
            var consoleDisplay = Kernel.Get<IConsoleDisplay>();
            var random = Kernel.Get<IRandom>();
            Bind<Register<Sharpmon>>().ToConstant(new Register<Sharpmon>
            {
                new Sharpmon("Magisharp", 5,
                    new List<Attack>
                    {
                        attackDex["Swift Swim"]
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Sharpmander", 10, baseAccuracy: 2,
                    attacks: new List<Attack>
                    {
                        attackDex["Scratch"],
                        attackDex["Grawl"]
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Sharpasaur", 9, baseAccuracy: 2, baseDodge: 3, baseSpeed: 2, 
                    attacks: new List<Attack>
                    {
                        attackDex["Pound"],
                        attackDex["Foliage"]
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Sharpitle", 11, baseDefense: 2, baseDodge: 2, baseSpeed: 2,
                    attacks: new List<Attack>
                    {
                        attackDex["Pound"],
                        attackDex["Shell"]
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Sharpichu", 10, basePower: 2, baseDodge: 2, baseAccuracy: 2, baseSpeed: 2,
                    attacks: new List<Attack>
                    {
                        attackDex["Thunderbolt"],
                        attackDex["Recharge"]
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Rattasharp", 6, baseDodge: 2, baseSpeed: 3,
                    attacks: new List<Attack>
                    {
                        attackDex["Bite"]
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Shpearow", 7, baseDodge: 3, baseAccuracy: 2, baseSpeed: 3,
                    attacks: new List<Attack>
                    {
                        attackDex["Claw"],
                        attackDex["Whirlwind"]
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Sharplax", 20, basePower: 2, baseDefense: 4,
                    attacks: new List<Attack>
                    {
                        attackDex["Rest"],
                        attackDex["Pound"],
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
                new Sharpmon("Sharpix", 8, basePower: 3, baseDefense: 2,
                    attacks: new List<Attack>
                    {
                        attackDex["Scratch"],
                        attackDex["Firebreathing"],
                        attackDex["Reheat"],
                    },
                    random: random,
                    consoleDisplay: consoleDisplay),
            });
        }
    }
}
