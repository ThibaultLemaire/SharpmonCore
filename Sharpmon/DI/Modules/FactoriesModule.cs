using Ninject.Modules;
using Sharpmon.Factories;
using Sharpmon.Interfaces;

namespace Sharpmon.DI.Modules
{
    public class FactoriesModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IFactory<Sharpmon, SharpmonRepr>>().To<SharpmonFactory>();
            Bind<IFactory<Player, PlayerRepr>>().To<PlayerFactory>();
        }
    }
}
