using Ninject.Modules;
using Sharpmon.Wrappers;
using Sharpmon.Interfaces;

namespace Sharpmon.DI.Modules
{
    public class RandomModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IRandom>().To<RandomWrapper>().InSingletonScope();
        }
    }
}
