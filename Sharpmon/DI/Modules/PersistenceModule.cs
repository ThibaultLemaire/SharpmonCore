using Ninject.Modules;
using Sharpmon.Interfaces;
using Sharpmon.FSharp;
using Sharpmon.Factories;

namespace Sharpmon.DI.Modules
{
    public class PersistenceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IStreamSerializer<Player>>().To<JsonStreamSerializer<Player, PlayerRepr>>();
            Bind<IPersister<Player>>().To<Persister<Player>>().InSingletonScope();
        }
    }
}
