using Ninject;
using Ninject.Modules;
using Sharpmon.Interfaces;

namespace Sharpmon.DI.Modules
{
    public class AttacksModule : NinjectModule
    {
        public override void Load()
        {
            var random = Kernel.Get<IRandom>();
            var console = Kernel.Get<IConsoleDisplay>();
            Bind<Register<Attack>>().ToConstant(new Register<Attack>
            {
                new Attack("Scratch", random, console, 3),
                new Attack("Grawl", random, console, power: 1, target: TargetType.SELF),
                new Attack("Pound", random, console, 2),
                new Attack("Foliage", random, console, dodge: 1, target: TargetType.SELF),
                new Attack("Shell", random, console, defense: 1, target: TargetType.SELF),
                new Attack("Swift Swim", random, console),
                new Attack("Thunderbolt", random, console, 1, dodge: 1),
                new Attack("Recharge", random, console, accuracy: 1, target: TargetType.SELF),
                new Attack("Whirlwind", random, console, 1, defense: 1),
                new Attack("Claw", random, console, 2),
                new Attack("Bite", random, console, 4),
                new Attack("Reheat", random, console, power: 2, target: TargetType.SELF),
                new Attack("Firebreathing", random, console, 1, 2),
                new Attack("Rest", random, console, 1, target: TargetType.SELF),
            });
        }
    }
}
