using Ninject;
using Ninject.Modules;
using Sharpmon.Interfaces;
using System.Collections.Generic;

namespace Sharpmon.DI.Modules
{
    public class PlayerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<NewPlayerBuilder>().ToConstant<NewPlayerBuilder>(
                (name, firstSharpmon, items, difficulty) =>
                new Player(
                    name: name,
                    sharpmons: new List<Sharpmon> {firstSharpmon},
                    currentSharpmon: firstSharpmon,
                    money: 0,
                    items: items,
                    difficulty: difficulty,
                    random: Kernel.Get<IRandom>(),
                    console: Kernel.Get<IConsoleDisplay>()
                ));
        }
    }
}
