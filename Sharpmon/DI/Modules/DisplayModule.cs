using Ninject.Modules;
using Sharpmon.Factories;
using Sharpmon.Interfaces;

namespace Sharpmon.DI.Modules
{
    public class DisplayModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IConsoleDisplay>().To<ConsoleDisplay>().InSingletonScope();
        }
    }
}
