using System;
using Ninject;
using Ninject.Modules;
using Ninject.Activation;
using Sharpmon.Interfaces;
using Sharpmon.FSharp;

namespace Sharpmon.DI.Modules
{
    public class ItemsModule : NinjectModule
    {
        public override void Load()
        {
            ItemByName itemByName = name => Kernel.Get<Item>(name);
            Bind<ItemByName>().ToConstant(itemByName);

            RegisterItem(
                "Sharpball",
                "Used to capture Sharpmons",
                price: 150
            );
            RegisterItem(
                "Potion",
                "Heals a Sharpmon 5HP",
                price: 100,
                damage: 5
            );
            RegisterItem(
                "Super Potion",
                "Heals a Sharpmon 10HP",
                price: 300,
                damage: 10
            );
            RegisterItem(
                "Rare Candy",
                "Makes a Sharpmon level up",
                price: 700,
                level: 1
            );
            RegisterItem(
                "Power Seed",
                "Permanently increases the Power of a Sharpmon by 1.",
                price: 1000,
                power: 1
            );
            RegisterItem(
                "Defense Seed",
                "Permanently increases the Defense of a Sharpmon by 1.",
                price: 1000,
                defense: 1
            );
            RegisterItem(
                "Dodge Seed",
                "Permanently increases the Dodge of a Sharpmon by 1.",
                price: 1000,
                dodge: 1
            );
            RegisterItem(
                "Accuracy Seed",
                "Permanently increases the Accuracy of a Sharpmon by 1.",
                price: 1000,
                accuracy: 1
            );
            RegisterItem(
                "Speed Seed",
                "Permanently increases the Speed of a Sharpmon by 1.",
                price: 1000,
                speed: 1
            );
            RegisterItem(
                "Mega Seed",
                "Permanently increases all stats of a Sharpmon by 1.",
                price: 4000,
                power: 1,
                defense: 1,
                dodge: 1,
                accuracy: 1,
                speed: 1
            );
        }

        void RegisterItem(
            string name,
            string description,
            int price,
            int damage = 0,
            int power = 0,
            int defense = 0,
            int dodge = 0,
            int accuracy = 0,
            int speed = 0,
            int level = 0,
            TargetType target = TargetType.SELF
        )
        {
            Func<IContext, Item> itemBuilder = 
                _ => new Item(
                    name: name,
                    sellPrice: price / 2,
                    damage: damage,
                    power: power,
                    defense: defense,
                    dodge: dodge,
                    accuracy: accuracy,
                    speed: speed,
                    level: level,
                    target: target,
                    count: 0,
                    random: Kernel.Get<IRandom>(),
                    sharpmonStatIncreased: Kernel.Get<SharpmonStatIncreased>(),
                    sharpmonLeveledUp: Kernel.Get<SharpmonLeveledUp>(),
                    sharpmonHealed: Kernel.Get<SharpmonHealed>()
                );
            Bind<IShopItem>().ToConstant(new ShopItem(name, price, description));
            Bind<Item>().ToMethod(itemBuilder).Named(name);
        }
    }
}
