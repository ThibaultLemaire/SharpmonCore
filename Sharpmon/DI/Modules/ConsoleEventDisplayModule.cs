using Ninject;
using Ninject.Modules;
using Sharpmon.Factories;
using Sharpmon.Interfaces;

namespace Sharpmon.DI.Modules
{
    public class ConsoleEventDisplayModule : NinjectModule
    {
        public override void Load()
        {
            var eventDisplayer = Kernel.Get<ConsoleEventDisplayer>();
            Bind<SharpmonStatIncreased>().ToConstant<SharpmonStatIncreased>(
                eventDisplayer.OnSharpmonStatIncreased);
            Bind<SharpmonLeveledUp>().ToConstant<SharpmonLeveledUp>(
                eventDisplayer.OnSharpmonLeveledUp);
            Bind<SharpmonHealed>().ToConstant<SharpmonHealed>(
                eventDisplayer.OnSharpmonHealed);
        }
    }
}
