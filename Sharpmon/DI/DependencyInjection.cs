using Ninject;
using Sharpmon.DI.Modules;

namespace Sharpmon.DI
{
    public static class DependencyInjection
    {
        public static IKernel Kernel()
        {
            return new StandardKernel(
                new RandomModule(),
                new DisplayModule(),
                new ConsoleEventDisplayModule(),
                new FactoriesModule(),
                new PlayerModule(),
                new ItemsModule(),
                new AttacksModule(),
                new SharpdexModule(),
                new MiscModule(),
                new PersistenceModule());
        }
    }
}
