using System;
using System.Linq.Expressions;

namespace Sharpmon
{
    public delegate void SharpmonStatIncreased(
        Sharpmon subject,
        Expression<Func<Sharpmon, int>> propertyPicker,
        int amount
    );

    public delegate void SharpmonLeveledUp(Sharpmon subject);

    public delegate void SharpmonHealed(
        Sharpmon subject,
        int amount
    );
}
