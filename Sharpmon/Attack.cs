﻿using System;
using Sharpmon.Interfaces;

namespace Sharpmon
{
    [Serializable()]
    public class Attack : Effect
    {
        public Attack(
            string name,
            IRandom random,
            IConsoleDisplay console,
            int damage = 0,
            int power = 0,
            int defense = 0,
            int dodge = 0,
            int accuracy = 0,
            int speed = 0,
            TargetType target = TargetType.ENEMY)
            :
            base(
                name,
                damage,
                power,
                defense,
                dodge,
                accuracy,
                speed,
                target,
                random,
                console)
        {

        }
    }
}
