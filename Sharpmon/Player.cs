﻿using System;
using System.Collections.Generic;
using Sharpmon.Interfaces;

namespace Sharpmon
{
    public delegate Player NewPlayerBuilder(
        string name,
        Sharpmon firstSharpmon,
        Dictionary<string, Item> itemsDex,
        string difficulty
    );

    public class Player
    {
        public IRandom random;
        public string Name { get; set; }
        public List<Sharpmon> Sharpmons { get; private set; }
        public Sharpmon CurrentSharpmon { get; set; }
        public int Money { get; set; }
        public Dictionary<string, Item> Items { get; private set; }
        public string Difficulty { get; private set; }

        IConsoleDisplay Console;

        public Player(
            string name,
            List<Sharpmon> sharpmons,
            Sharpmon currentSharpmon,
            int money,
            Dictionary<string, Item> items,
            string difficulty,
            IRandom random,
            IConsoleDisplay console)
        {
            Name = name;
            Sharpmons = sharpmons;
            CurrentSharpmon = currentSharpmon;
            Money = money;
            Items = items;
            Difficulty = difficulty;
            this.random = random;
            Console = console;
        }

        public override string ToString()
        {
            return Name;
        }

        public bool Capture(Sharpmon target)
        {
            Items["Sharpball"].Count--;

            Console.WriteLine(ConsoleColor.White, "You used Sharpball on {0}", target);

            if (random.Next(100) < (((double)(target.MaxHP - target.HP) / target.MaxHP) - 0.5) * 100)
            {
                Sharpmons.Add(target);
                Console.WriteLine(ConsoleColor.Cyan, "{0} has been added to your party!", target);
                return true;
            }
            else
            {
                Console.WriteLine(ConsoleColor.Blue, "You failed to capture {0}.", target);
                return false;
            }
        }

        public bool RunAwayFrom(Sharpmon opponent)
        {
            return random.Next(100) < ((CurrentSharpmon.Speed / (double)(CurrentSharpmon.Speed + opponent.Speed)) + 0.1) * 100;
        }

        public int GetAliveSharpmonsCount()
        {
            int count = 0;

            foreach (Sharpmon sharpmon in Sharpmons)
            {
                if (sharpmon.HP > 0)
                {
                    count++;
                }
            }

            return count;
        }

        public void SwitchCurrentSharpmon(Sharpmon newCurrent)
        {
            CurrentSharpmon = newCurrent;
            Console.WriteLine(ConsoleColor.White, "Go {0}!", CurrentSharpmon);
        }

    }
}

