using System;
using System.Collections.Generic;
using Ninject;
using Sharpmon.DI;
using Sharpmon.Interfaces;

namespace Sharpmon
{
    public class Program
    {
        static Version Version => new Version(3, 14, 0);

        static void Main(string[] args)
        {
            var kernel = DependencyInjection.Kernel();
            new CommandLineInterface(
                kernel.Get<IConsoleDisplay>(),
                kernel.Get<string>("AppName"),
                Version,
                kernel.Get<IRandom>(),
                kernel.Get<IPersister<Player>>(),
                kernel.Get<Register<Sharpmon>>(),
                kernel.Get<List<IShopItem>>(),
                kernel.Get<ItemByName>(),
                kernel.Get<NewPlayerBuilder>()
            ).Start();
        }
    }
}
