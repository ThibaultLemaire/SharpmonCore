using System.Runtime.Serialization;

namespace Sharpmon
{
    static class SerializationInfoExtensions
    {
        public static void Add<T>(this SerializationInfo info, string name, T value)
        {
            info.AddValue(name, value, typeof(T));
        }

        public static T Get<T>(this SerializationInfo info, string name)
        {
            return (T)info.GetValue(name, typeof(T));
        }
    }
}