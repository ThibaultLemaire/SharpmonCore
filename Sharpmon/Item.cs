﻿using System;
using Sharpmon.Interfaces;

namespace Sharpmon
{
    public delegate Item ItemByName(string name);

    [Serializable()]
    public class Item : Effect
    {
        public int SellPrice { get; private set; }
        private int Level { get; set; }
        public int Count { get; set; }
        readonly SharpmonStatIncreased sharpmonStatIncreased;
        readonly SharpmonLeveledUp sharpmonLeveledUp;
        readonly SharpmonHealed sharpmonHealed;

        public Item(
            string name,
            int sellPrice,
            int damage,
            int power,
            int defense,
            int dodge,
            int accuracy,
            int speed,
            int level,
            TargetType target,
            int count,
            IRandom random,
            SharpmonStatIncreased sharpmonStatIncreased,
            SharpmonLeveledUp sharpmonLeveledUp,
            SharpmonHealed sharpmonHealed
        ) : base(
            name, 
            damage, 
            power, 
            defense, 
            dodge, 
            accuracy, 
            speed, 
            target,
            random,
            console: null
        )
        {
            SellPrice = sellPrice;
            Level = level;
            Count = count;
            this.sharpmonStatIncreased = sharpmonStatIncreased;
            this.sharpmonLeveledUp = sharpmonLeveledUp;
            this.sharpmonHealed = sharpmonHealed;
        }

        public bool Activate(Sharpmon target)
        {
            bool hasEffect = false;

            if (Level > 0)
            {
                hasEffect = true;
                for (int i = 1; i <= Level; i++)
                {
                    target.LevelUp();
                }
                sharpmonLeveledUp(target);
            }

            if (Damage > 0)
            {
                hasEffect = true;
                int previousHP = target.HP;

                target.HP += Damage;
                if (target.HP > target.MaxHP)
                {
                    target.HP = target.MaxHP;
                }

                sharpmonHealed(target, target.HP - previousHP);
            }

            if (Accuracy > 0)
            {
                hasEffect = true;
                target.BaseAccuracy += Accuracy;
                sharpmonStatIncreased(target, x => x.Accuracy, Accuracy);
            }
            if (Defense > 0)
            {
                hasEffect = true;
                target.BaseDefense += Defense;
                sharpmonStatIncreased(target, x => x.Defense, Defense);
            }
            if (Dodge > 0)
            {
                hasEffect = true;
                target.BaseDodge += Dodge;
                sharpmonStatIncreased(target, x => x.Dodge, Dodge);
            }
            if (Power > 0)
            {
                hasEffect = true;
                target.BasePower += Power;
                sharpmonStatIncreased(target, x => x.Power, Power);
            }
            if (Speed > 0)
            {
                hasEffect = true;
                target.BaseSpeed += Speed;
                sharpmonStatIncreased(target, x => x.Speed, Speed);
            }

            return hasEffect;
        }
    }
}
