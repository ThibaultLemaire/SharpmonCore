﻿using System;
using Sharpmon.FSharp;
using Sharpmon.Interfaces;

namespace Sharpmon
{
    public enum TargetType { SELF, ENEMY };

    [Serializable()]
    public abstract class Effect
    {
        protected readonly IRandom random;
        public string Name { get; set; }
        public int Damage { get; set; }
        public int Power { get; set; }
        public int Defense { get; set; }
        public int Dodge { get; set; }
        public int Accuracy { get; set; }
        public int Speed { get; set; }
        public TargetType Target { get; set; }

        protected readonly IConsoleDisplay Console;

        public Effect(
            string name,
            int damage,
            int power,
            int defense,
            int dodge,
            int accuracy,
            int speed,
            TargetType target,
            IRandom random,
            IConsoleDisplay console)
        {
            Name = name;
            Damage = damage;
            Power = power;
            Defense = defense;
            Dodge = dodge;
            Accuracy = accuracy;
            Speed = speed;
            Target = target;
            this.random = random;
            Console = console;
        }

        public override string ToString()
        {
            return Name;
        }

        public bool Buff(Sharpmon target, bool debuff = false)
        {
            bool hasEffect = false;

            if (!debuff && Damage > 0)
            {
                hasEffect = true;
                if (target.HP == target.MaxHP)
                {
                    Console.WriteLine(ConsoleColor.White, "{0} is already at full health", target);
                }
                else
                {
                    int previousHP = target.HP;

                    target.HP += Damage;
                    if (target.HP > target.MaxHP)
                    {
                        target.HP = target.MaxHP;
                    }
                    Console.WriteLine(ConsoleColor.Green, "{0} is restored {1} HP", target, target.HP - previousHP);
                }
            }

            if (Accuracy > 0)
            {
                hasEffect = true;
                if (target.Accuracy == 1 && debuff)
                {
                    Console.WriteLine(ConsoleColor.White, "{0} is already at minimum Accuracy.", target);
                }
                else
                {
                    int previousvalue = target.Accuracy;
                    target.Accuracy += (debuff ? -Accuracy : Accuracy);
                    if (target.Accuracy < 1)
                    {
                        target.Accuracy = 1;

                    }
                    Console.WriteLine(ConsoleColor.DarkYellow, "{0}'s Accuracy {1} by {2}.", target, (debuff ? "decreased" : "increased"), (debuff ? previousvalue - target.Accuracy : target.Accuracy - previousvalue));
                }
                
            }
            if (Defense > 0)
            {
                hasEffect = true;
                if (target.Defense == 1 && debuff)
                {
                    Console.WriteLine(ConsoleColor.White, "{0} is already at minimum Defense.", target);
                }
                else
                {
                    int previousvalue = target.Defense;
                    target.Defense += (debuff ? -Defense : Defense);
                    if (target.Defense < 1)
                    {
                        target.Defense = 1;

                    }
                    Console.WriteLine(ConsoleColor.DarkGreen, "{0}'s Defense {1} by {2}.", target, (debuff ? "decreased" : "increased"), (debuff ? previousvalue - target.Defense : target.Defense - previousvalue));
                }
                
            }
            if (Dodge > 0)
            {
                hasEffect = true;
                if (target.Dodge == 1 && debuff)
                {
                    Console.WriteLine(ConsoleColor.White, "{0} is already at minimum Dodge.", target);
                }
                else
                {
                    int previousvalue = target.Dodge;
                    target.Dodge += (debuff ? -Dodge : Dodge);
                    if (target.Dodge < 1)
                    {
                        target.Dodge = 1;
                    }
                    Console.WriteLine(ConsoleColor.DarkMagenta, "{0}'s Dodge {1} by {2}.", target, (debuff ? "decreased" : "increased"), (debuff ? previousvalue - target.Dodge : target.Dodge - previousvalue));
                }
            }
            if (Power > 0)
            {
                hasEffect = true;
                if (target.Power == 1 && debuff)
                {
                    Console.WriteLine(ConsoleColor.White, "{0} is already at minimum Power.", target);
                }
                else
                {
                    int previousvalue = target.Power;
                    target.Power += (debuff ? -Power : Power);
                    if (target.Power < 1)
                    {
                        target.Power = 1;
                    }
                    Console.WriteLine(ConsoleColor.DarkRed, "{0}'s Power {1} by {2}.", target, (debuff ? "decreased" : "increased"), (debuff ? previousvalue - target.Power : target.Power - previousvalue));
                }
            }
            if (Speed > 0)
            {
                hasEffect = true;
                if (target.Speed == 1 && debuff)
                {
                    Console.WriteLine(ConsoleColor.White, "{0} can't be slowed any more.", target);
                }
                else
                {
                    int previousvalue = target.Speed;
                    target.Speed += (debuff ? -Speed : Speed);
                    if (target.Speed < 1)
                    {
                        target.Speed = 1;
                    }
                    Console.WriteLine(ConsoleColor.DarkCyan, "{0}'s Speed {1} by {2}.", target, (debuff ? "decreased" : "increased"), (debuff ? previousvalue - target.Speed : target.Speed - previousvalue));
                }
                
            }
            
            return hasEffect;
        }

        public bool Activate(Sharpmon launcher, Sharpmon opponent)
        {
            bool hasEffect = false;

            Console.WriteLine("{0} launches {1}.", launcher, Name);

            if (Target == TargetType.SELF)
            {
                Sharpmon target = launcher;
                hasEffect = Buff(target);
            }
            else
            {
                Sharpmon target = opponent;
                if (random.Next(100) < ((launcher.Accuracy / (double)(launcher.Accuracy + target.Dodge)) + 0.1) * 100)
                {
                    if (Damage > 0)
                    {
                        hasEffect = true;
                        target.ReceiveDamage(random.Round( (launcher.Power * Damage) / (double)target.Defense ) );
                    }

                    hasEffect = Buff(target, true) || hasEffect;
                }
                else
                {
                    Console.WriteLine(ConsoleColor.Blue, "The attack misses.");
                    hasEffect = true;
                }
            }

            return hasEffect;
        }
    }
}
