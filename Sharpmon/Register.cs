using System.Collections;
using System.Collections.Generic;

namespace Sharpmon
{
    public class Register<T> : IReadOnlyDictionary<string, T>, ICollection<T>
    {
        Dictionary<string, T> internalDictionary = new Dictionary<string, T>();

        public Register()
        {

        }

        public Register(IEnumerable<T> seed)
        {
            foreach (var item in seed)
            {
                Add(item);
            }
        }

        #region IReadOnlyDictionary<string, T>

        public T this[string key] => ((IReadOnlyDictionary<string, T>)internalDictionary)[key];

        public IEnumerable<string> Keys => ((IReadOnlyDictionary<string, T>)internalDictionary).Keys;

        public IEnumerable<T> Values => ((IReadOnlyDictionary<string, T>)internalDictionary).Values;

        public int Count => ((IReadOnlyDictionary<string, T>)internalDictionary).Count;

        public bool ContainsKey(string key)
        {
            return ((IReadOnlyDictionary<string, T>)internalDictionary).ContainsKey(key);
        }

        public IEnumerator<KeyValuePair<string, T>> GetEnumerator()
        {
            return ((IReadOnlyDictionary<string, T>)internalDictionary).GetEnumerator();
        }

        public bool TryGetValue(string key, out T value)
        {
            return ((IReadOnlyDictionary<string, T>)internalDictionary).TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IReadOnlyDictionary<string, T>)internalDictionary).GetEnumerator();
        }

        #endregion

        #region ICollection<T>

        public bool IsReadOnly => ((ICollection<T>)internalDictionary.Values).IsReadOnly;

        public void Add(T item)
        {
            internalDictionary.Add(item.ToString(), item);
        }

        public bool Remove(T item)
        {
            return internalDictionary.Remove(item.ToString());
        }

        public void Clear()
        {
            internalDictionary.Clear();
        }

        public bool Contains(T item)
        {
            return internalDictionary.ContainsKey(item.ToString());
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            ((ICollection<T>)internalDictionary.Values).CopyTo(array, arrayIndex);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return ((ICollection<T>)internalDictionary.Values).GetEnumerator();
        }

        #endregion
    }
}