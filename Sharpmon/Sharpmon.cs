﻿using System;
using System.Collections.Generic;
using Sharpmon.Interfaces;
using Sharpmon.FSharp;

namespace Sharpmon
{
    public class Sharpmon
    {
        public string Name { get; set; }
        public int Level { get; set; }
        int xp;
        public int XP
        {
            get => xp;
            set
            {
                xp += value;
                while (xp >= XPToNextLevel)
                {
                    xp -= XPToNextLevel;
                    LevelUp();
                }
            }
        }
        public int XPToNextLevel { get; set; }
        public int MaxHP { get; set; }
        public int HP { get; set; }
        public int BasePower { get; set; }
        public int Power { get; set; }
        public int BaseDefense { get; set; }
        public int Defense { get; set; }
        public int BaseDodge { get; set; }
        public int Dodge { get; set; }
        public int BaseAccuracy { get; set; }
        public int Accuracy { get; set; }
        public int BaseSpeed { get; set; }
        public int Speed { get; set; }
        public List<Attack> Attacks { get; set; }

        IConsoleDisplay Console;
        IRandom random;

        public Sharpmon(
            string name,
            int level,
            int xp,
            int xpToNextLevel,
            int maxHp,
            int hp,
            int basePower,
            int power,
            int baseDefense,
            int defense,
            int baseDodge,
            int dodge,
            int baseAccuracy,
            int accuracy,
            int baseSpeed,
            int speed,
            List<Attack> attacks,
            IRandom random,
            IConsoleDisplay consoleDisplay
        )
        {
            Name = name;
            Level = level;
            this.xp = xp;
            XPToNextLevel = xpToNextLevel;
            MaxHP = maxHp;
            HP = hp;
            BasePower = basePower;
            Power = power;
            BaseDefense = baseDefense;
            Defense = defense;
            BaseDodge = baseDodge;
            Dodge = dodge;
            BaseAccuracy = baseAccuracy;
            Accuracy = accuracy;
            BaseSpeed = baseSpeed;
            Speed = speed;
            Attacks = attacks;
            Console = consoleDisplay;
            this.random = random;
        }

        public Sharpmon(
            string name,
            int maxHP,
            List<Attack> attacks,
            IRandom random,
            IConsoleDisplay consoleDisplay,
            int basePower = 1,
            int baseDefense = 1,
            int baseDodge = 1,
            int baseAccuracy = 1,
            int baseSpeed = 1)
        {
            Name = name;
            Level = 1;
            XPToNextLevel = 500;
            xp = 0;
            MaxHP = maxHP;
            HP = maxHP;
            BasePower = basePower;
            Power = basePower;
            BaseDefense = baseDefense;
            Defense = baseDefense;
            BaseDodge = baseDodge;
            Dodge = baseDodge;
            BaseAccuracy = baseAccuracy;
            Accuracy = baseAccuracy;
            BaseSpeed = baseSpeed;
            Speed = baseSpeed;
            Attacks = attacks;
            Console = consoleDisplay;
            this.random = random;
        }

        public Sharpmon(Sharpmon other)
        {
            Name = other.Name;
            Level = other.Level;
            xp = other.xp;
            XPToNextLevel = other.XPToNextLevel;
            MaxHP = other.MaxHP;
            HP = other.HP;
            BasePower = other.BasePower;
            Power = other.Power;
            BaseDefense = other.BaseDefense;
            Defense = other.Defense;
            BaseDodge = other.BaseDodge;
            Dodge = other.Dodge;
            BaseAccuracy = other.BaseAccuracy;
            Accuracy = other.Accuracy;
            BaseSpeed = other.BaseSpeed;
            Speed = other.Speed;
            Attacks = other.Attacks;
            Console = other.Console;
            random = other.random;
        }

        public override string ToString()
        {
            return Name;
        }

        public void LevelUp()
        {
            int previousMaxHP = MaxHP;
            MaxHP = random.Round(1.3 * MaxHP);
            HP = random.Round((double)HP / previousMaxHP * MaxHP);
            BaseAccuracy = random.Round(1.3 * BaseAccuracy);
            BaseDefense = random.Round(1.3 * BaseDefense);
            BaseDodge = random.Round(1.3 * BaseDodge);
            BasePower = random.Round(1.3 * BasePower);
            BaseSpeed = random.Round(1.3 * BaseSpeed);
            Level++;
            XPToNextLevel = random.Round(XPToNextLevel * Level * 1.3);
        }

        public void Reset()
        {
            Power = BasePower;
            Defense = BaseDefense;
            Dodge = BaseDodge;
            Accuracy = BaseAccuracy;
            Speed = BaseSpeed;
        }

        public void Rename()
        {
            string newName = "";

            Console.WriteLine("Enter a new name for {0}: ", Name);
            newName = Console.ReadLine();

            if (newName != "")
            {
                Console.WriteLine(ConsoleColor.White, "{0}'s name is now {1}", Name, newName);

                Name = newName;
            }
            else Console.WriteLine("{0} hasn't been renamed.", Name);
        }

        public void ReceiveDamage(int damage)
        {
            if (damage < 1)
            {
                damage = 1;
            }
            HP -= damage;
            if (HP < 1)
            {
                HP = 0;
                Console.WriteLine(ConsoleColor.Red, "{0} {1}.", Name, (CommandLineInterface.Difficulty == "Normal"? "fainted" : "is dead") );
            }
            else Console.WriteLine(ConsoleColor.Red, "{0} loses {1} health", Name, damage);
        }
    }
}
